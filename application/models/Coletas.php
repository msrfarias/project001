<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 27/01/2018
 * Time: 00:25
 */

class Coletas extends CI_Model
{



    function getAll()
    {
        $this->load->database();
        //$this->db->order_by("");
        $this->db->from(_BD_."compras as c");
        $this->db->join(_BD_."funcionarios as f","c.funcionario_cpf = f.cpf");
        $this->db->join(_BD_."pessoas as p","p.cpf = c.pessoa_cpf");
        $this->db->where("c.status","COLETA");
        $this->db->order_by("f.nome,p.nome");
        $result = $this->db->get();
        if ($result->num_rows())
            return $result->result();
        return [];
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getFuncionario()
    {
        return $this->funcionario;
    }

    /**
     * @param mixed $funcionario
     */
    public function setFuncionario($funcionario)
    {
        $this->funcionario = $funcionario;
    }

    /**
     * @return mixed
     */
    public function getProdutor()
    {
        return $this->produtor;
    }

    /**
     * @param mixed $produtor
     */
    public function setProdutor($produtor)
    {
        $this->produtor = $produtor;
    }

    /**
     * @return mixed caixas
     */
    public function getCaixas()
    {
        return $this->caixas;
    }

    /**
     * @param mixed $caixa
     */
    public function setCaixas($caixas)
    {
        $this->caixas = $caixas;
    }



}