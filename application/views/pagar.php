<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div id="success"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Pagamento<small></small></h2>
                    <ul class="nav navbar-left panel_toolbox">
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="search" id="search" class="form-control" placeholder="Buscar Produtor" style="width:200px; ">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Buscar</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="add-on" href="<?=URL_PUBLIC?>/produtores/cadastro"><i class="fa fa-plus"></i></a>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">



                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action table-hover table-condensed" id="table">
                            <thead>
                            <tr class="headings">
                                <th>
                                    
                                </th>
                                <th class="column-title">Nome</th>
                                <th class="column-title">C.P.F </th>
                                <th class="column-title no-link last"><span class="nobr">Data</span>
                                <th class="column-title">Total <span id="totalSelecionados"></span></th>
                                <th class="column-title">Status</th>
                                <th class="column-title no-link last"><span class="nobr"></span>
                                </th>
                                <th class="bulk-actions" colspan="7">
                                    <a class="antoo" style="color:#fff; font-weight:500;">Selecionados ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($compras as $compra):?>
                                <tr class="even pointer accordion-toggle linha linhaC" data-toggle="collapse" data-parent="#OrderPackages" data-target=".packageDetails1<?= $compra->id ?>">
                                    <td class="a-center ">

                                    </td>
                                    <td class=" "><?= $compra->produtor->nome ?></td>
                                    <td class=" "><?= $compra->pessoa_cpf ?></td>
                                    <td class=" "><?= date('d-m-Y',  strtotime($compra->data)) ?></td>
                                    <td class="jq_valorCompra">R$ <?= number_format($compra->total->verde->peso + $compra->total->madura->peso,2,","," ") ?></td>
                                    <td class=""><?= $compra->status ?></td>
                                    <td class=" ">
                                        <a class="jq_send" href="#">
                                            <form class="frm_confirm" message="Confirma a exclusao desta compra?" titulo="Excluir compra?" action="<?=URL_PUBLIC?>/compra/remover">
                                                <input type="hidden" value="<?= $compra->id ?>" name="id_compra">
                                            </form>
                                            <i class="success fa fa-trash"></i>
                                        </a>

                                        <a href="#" class="btnRecibo" idc="<?=$compra->id?>">
                                            <i class="success fa fa-usd" style="margin-left:40%"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr class="hiddenRow linhaEscondida">
                                    <td></td>
                                    <td colspan="5" >
                                        <div class="accordion-body collapse packageDetails1<?= $compra->id ?>" id="accordion1">
                                            <table class="table jambo_table" >
                                            <?php if (count($compra->itens)) :?>
                                                    <tr><th colspan="7"><h6><b>Itens:</b></h6></th></tr>
                                                    <head>
                                                        <th colspan="2">Tipo</th>
                                                        <th>Caixas</th>
                                                        <th>Peso</th>
                                                        <th>Cotação</th>
                                                        <th>Total </th>
                                                        <th>Ações</th>
                                                    </head>
                                                    <?php foreach ($compra->itens as $iten):?>
                                                        <tr>
                                                            <td colspan="2"><?=$iten->tipo?></td>
                                                            <td><?=$iten->caixas?></td>
                                                            <td><?=$iten->peso?></td>
                                                            <td>R$ <?=$iten->cotacao?></td>
                                                            <td>R$ <?=$iten->total_peso?></td>
                                                            <td>

                                                                <a href="" class="jq_edit" cod=""><i class="fa fa-edit"></i>
                                                                        <input type="hidden" class="jq_edit_id" value="<?= $iten->id ?>">
                                                                        <input type="hidden" class="jq_edit_cotacao" value="<?= $iten->cotacao ?>" >
                                                                        <input type="hidden" class="jq_edit_peso" value="<?= str_replace(",",".",$iten->peso) ?>" >
                                                                </a>
                                                            </td>

                                                        </tr>
                                                    <?php endforeach;?>
                                            <?php endif?>
                                            <?php if (count($compra->produtor->adiantamentos)) :?>
                                                    <tr><th colspan="7"><h6><b>Adiantamentos:</b></h6></th></tr>
                                                    <head>
                                                        <th colspan="2">Data</th>
                                                        <th>Valor</th>
                                                        <th>Aplicar</th>
                                                        <th>Restante</th>
                                                        <th></th>
                                                        <th>Ações</th>
                                                    </head>
                                                <?php foreach ($compra->produtor->adiantamentos as $adiantamento):?>
                                                    <tr class="val" des="<?= $adiantamento->restante?>" ida="<?=$adiantamento->id?>">
                                                        <td colspan="2"><?= date("d-m-Y",strtotime($adiantamento->data))?></td>
                                                        <td>R$ <?= number_format($adiantamento->valor,2,","," ")?></td>
                                                        <td><span class="aplyDesconto">R$ 0,00</span></td>
                                                        <td><span class="restante_td"><?="R$ ".number_format($adiantamento->restante,2,","," ")?></span></td>
                                                        <td></td>
                                                        <td>
                                                            <a href="" class="jq_resetAdiantamento">
                                                                <i class="fa fa-refresh"></i>
                                                            </a>
                                                            <a href="" class="jq_adiantamento" codigo="<?=$adiantamento->id?>">
                                                                <i class="fa fa-tag" style="margin-left: 20%"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach?>
                                            <?php endif;?>
                                            </table>

                                        </div>
                                    </td>
                                    <td></td>
                                </tr>

                            <?php endforeach ?>
                            </tbody>
                        </table>

                        <div id="modal_edit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">


                                    <form id="" class="form-horizontal form-label-left input_mask frm_edit" action="">
                                        <fieldset class="col-md-6" style="margin-top: 0.5%; margin-right: 10%">
                                            <legend>Cotação</legend>

                                            <div class="panel panel-default">
                                                <div class="panel-body">

                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control" id="jq_cotacao" placeholder="Cotação" name="cotacao" required="required" >
                                                        <span class="fa fa-usd form-control-feedback right" aria-hidden="true"></span>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control" id="jq_peso" placeholder="Peso" name="peso" >
                                                        <span class="fa form-control-feedback right" aria-hidden="true">KG</span>
                                                    </div>
                                                </div>
                                        </fieldset>
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-primary ">Confirmar</button>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="modal_recibo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Recibo de Pagamento <strong><span id="nome_delete"></span></strong></h4>
                                    </div>
                                    <form id="confirm_paga"  action="">
                                        <input type="hidden" id="idc_recibo" name="id_compra">
                                        <div id="hiddenFields">

                                        </div>
                                        <div class="modal-body">
                                            <div class="panel panel-default">
                                                <div class="panel-body" id="recibo-modal">                                              </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button id="pagerar" class="btn btn-primary">Pagar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div id="modal_adiantamento" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">


                                    <form id="formdesconto" class="form-horizontal form-label-left input_mask" action="">
                                        <fieldset class="col-md-6" style="margin-top: 0.5%; margin-right: 10%">
                                            <legend>Adiantamento</legend>

                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input type="hidden" id="restante" value="">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control desconto" value="" placeholder="Valor Descontado" id="valor_desc" restante="" name="desconto" required="required" >
                                                        <span class="fa fa-usd form-control-feedback right" aria-hidden="true"></span>
                                                    </div>
                                                </div>
                                        </fieldset>
                                        <div class="modal-header">
                                            <button type="button" class="close clsMA" data-dismiss="modal"><span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default clsMA" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" class="btn btn-primary ">Confirmar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
