
<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recibo de Pagamento</h2>
<p>Data:<?= date("d-m-Y",strtotime($adiantamento->data))?>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nº<?=$adiantamento->id?></p><br>
<div style="text-align: justify">
    Eu,<?= $produtor->nome?>, brasileiro, inscrito no CPF sob <br>
    o nº <?= $produtor->cpf ?>, <strong>DECLARO TER RECEBIDO DA EMPRESA</strong><br>
    Fernando Acerola, pessoa júridica  de direito privado, inscrita <br>
    no CNPJ sob o nº XXXXXXXXXXXXX, a importância de R$ <span id="totalAd"><?= number_format($adiantamento->valor,2,","," ")?></span><br>
    ( <span id="totalExtensoAd"></span>), referente  ao adiantamento de <br>
    pagamento.
</div>
<br>
<br>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;___________________________<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Assinatura do Recebedor</strong></p>
</p>


