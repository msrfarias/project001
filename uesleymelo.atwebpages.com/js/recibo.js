//var element = document.getElementById('recibo-modal');

function geraPdf(element){
    html2pdf(element, {
        margin:       0.4,
        filename:     'myfile.pdf',
        image:        { type: 'jpeg', quality: 1.0 },
        html2canvas:  { dpi: 300, letterRendering: true },
        jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
    });
}

$("#gerarCompra").click(function(event){

    event.preventDefault();
    geraPdf(document.getElementById("recibo-modal"));
    //location.reload();
    $("#recibo-modal").html("");
    $("#modal_recibo").modal('hide');
});

$("#gerarAdiantamento").click(function (e) {
    e.preventDefault();
    geraPdf(document.getElementById("reciboAd"))
    $("#reciboAd").html("");
    $("#modal_reciboA").modal('hide');
});


$("#pagerar").click(function (ev) {
    ev.preventDefault();
    geraPdf(document.getElementById("recibo-modal"));
    $.ajax({
        url : "http://"+location.hostname+"/compra/pagar",
        method: "POST",
        assinc: false,
        cache : false,
        data : $("#confirm_paga").serialize(),
        success : function (data) {
            console.log(data);
            $("#modal_recibo").modal('hide');
            var idc = $("#idc_recibo").val();
            idc = ".btnRecibo[idc="+idc+"]:eq(0)";
            var linha = $(idc).parents("tr:eq(0)");
            linha.hide();
            linha.next("tr").hide();
            alertt(data,true);
            //location.reload(true);
        },
        error : function (data) {
            console.log(data);
            alert(data.responseText,false);
        }
    });
    // location.reload();
});




$(".verRecibo").click(function (e) {
    e.preventDefault();
    $("#modal_reciboA").modal();
    $.post($(this).attr("href"),{"adiantamento" : $(this).attr("cod")},function(data){
        $("#reciboAd").html(data);
        $("#totalExtensoAd").html($("#totalAd").html().extenso());
    });
});



$(".btnRecibo").click(function (e) {
    e.preventDefault();
    var linha = $(this).parents("tr:eq(0)").next();

    var dscArray = linha.find("tr[apl]");
    var descontos = [];
    var adiantamentos = [];

    var hiddenFields = $("#hiddenFields");
    hiddenFields.html("");
    dscArray.each(function () {
        descontos.push($(this).attr("apl"));
        adiantamentos.push($(this).attr("ida"));
        hiddenFields.append($("<input type='hidden' name='descontos[]' value='"+$(this).attr("apl")+"'>"));
        hiddenFields.append($("<input type='hidden' name='adiantamentos[]' value='"+$(this).attr("ida")+"'>"));
    });

    getDadosRecibo($(this).attr("idc"),descontos,adiantamentos);
    $("#idc_recibo").val($(this).attr("idc"));

});


//+ Carlos R. L. Rodrigues
//@ http://jsfromhell.com/string/extenso [rev. #3]
String.prototype.extenso = function(c){
    c = true;
    var ex = [
        ["zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"],
        ["dez", "vinte", "trinta", "quarenta", "cinqüenta", "sessenta", "setenta", "oitenta", "noventa"],
        ["cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"],
        ["mil", "milhão", "bilhão", "trilhão", "quadrilhão", "quintilhão", "sextilhão", "setilhão", "octilhão", "nonilhão", "decilhão", "undecilhão", "dodecilhão", "tredecilhão", "quatrodecilhão", "quindecilhão", "sedecilhão", "septendecilhão", "octencilhão", "nonencilhão"]
    ];
    var a, n, v, i, n = this.replace(c ? /[^,\d]/g : /\D/g, "").split(","), e = " e ", $ = "real", d = "centavo", sl;
    for(var f = n.length - 1, l, j = -1, r = [], s = [], t = ""; ++j <= f; s = []){
        j && (n[j] = (("." + n[j]) * 1).toFixed(2).slice(2));
        if(!(a = (v = n[j]).slice((l = v.length) % 3).match(/\d{3}/g), v = l % 3 ? [v.slice(0, l % 3)] : [], v = a ? v.concat(a) : v).length) continue;
        for(a = -1, l = v.length; ++a < l; t = ""){
            if(!(i = v[a] * 1)) continue;
            i % 100 < 20 && (t += ex[0][i % 100]) ||
            i % 100 + 1 && (t += ex[1][(i % 100 / 10 >> 0) - 1] + (i % 10 ? e + ex[0][i % 10] : ""));
            s.push((i < 100 ? t : !(i % 100) ? ex[2][i == 100 ? 0 : i / 100 >> 0] : (ex[2][i / 100 >> 0] + e + t)) +
                ((t = l - a - 2) > -1 ? " " + (i > 1 && t > 0 ? ex[3][t].replace("ão", "ões") : ex[3][t]) : ""));
        }
        a = ((sl = s.length) > 1 ? (a = s.pop(), s.join(" ") + e + a) : s.join("") || ((!j && (n[j + 1] * 1 > 0) || r.length) ? "" : ex[0][0]));
        a && r.push(a + (c ? (" " + (v.join("") * 1 > 1 ? j ? d + "s" : (/0{6,}$/.test(n[0]) ? "de " : "") + $.replace("l", "is") : j ? d : $)) : ""));
    }
    return r.join(e);
}


function AddTableline(content,table) {

    var newRow = $("<tr>");
    var cols = "";
    for (i=0;i<content.length;i++){
        cols += "<td>"+content[i]+"</td>";
    }
    newRow.append(cols);
    table.append(newRow);
}

function getDadosRecibo(id_compra,descontos,adiantamentos){

    $.ajax({
        url : "http://"+location.hostname+"/compra/recibo/",
        method : "POST",
        cache : false,
        type : "JSON",
        data :{
            "id_compra" : id_compra,
            "descontos"  : descontos,//$("#id_adiantamento")  .val() ,// coloca aqui o id do desconto
            "adiantamentos"      : adiantamentos//$("#desconto").val() /// coloca aqui o valor do desconto
        },
        success : function (compra) {
            $("#recibo-modal").html(compra);
            $("#modal_recibo").modal("show");
            $("#extensoRecibo").html($("#totalRecibo").html().extenso());
        },
        error: function(data){
            console.log("erro: ");
            console.log(data);
            alert("error: ");
        }

    });
}
$(".ParcelasPg").click(function() {
    var id = $(this).attr("Parcela-compra");
    getDadosRecibo(id,null,null);
});
