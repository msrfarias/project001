<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div id="success"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tabela de Produtores
                        <small></small>
                    </h2>
                    <ul class="nav navbar-left panel_toolbox">
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="search" id="search" class="form-control" placeholder="Buscar"
                                           style="width:200px; ">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Buscar</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="add-on" href="<?= URL_PUBLIC ?>/produtores/cadastro"><i
                                        class="fa fa-plus"></i></a>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">


                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action table-hover table-condensed"
                               id="table">
                            <thead>
                            <tr class="headings">

                                <th class="column-title">Nome</th>
                                <th class="column-title">C.P.F</th>
                                <th class="column-title">Observação</th>
                                <th class="column-title no-link last"><span class="nobr"></span>
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($produtores as $produtor): ?>
                            <tr class="even pointer accordion-toggle linha linhaC" data-toggle="collapse"
                                data-parent="#OrderPackages"
                                data-target=".packageDetails1<?= str_replace("-", "", str_replace(".", "", $produtor->cpf)) ?>">

                                <td class=" jq_nome"><?= $produtor->nome ?></td>
                                <td class=" "><?= $produtor->cpf ?></td>
                                <td class=" "><?= $produtor->observacao ?></td>
                                <td class=" "><a class="jq_delete" cpf="<?= $produtor->cpf ?>" href="">
                                                <i class="success fa fa-trash"></i></a>
                                              <a href="<?= URL_PUBLIC ?>/produtores/alterar/<?= $produtor->cpf?>">
                                                  <i class="success fa fa-edit" style="margin-left:40%"></i></a></td>
                                </td>
                            </tr>
                            <tr class="hiddenRow linhaEscondida">
                                <?php if (count($produtor->lotes)) : ?>
                                    <td colspan="3">
                                        <div class="accordion-body collapse packageDetails1<?= str_replace("-", "", str_replace(".", "", $produtor->cpf)) ?>">

                                            <table class=" table jambo_table">
                                                <head>
                                                    <th>Lote</th>
                                                    <th>Núcleo</th>
                                                    <th>Endereço</th>
                                                    <th>Área Plantada</th>
                                                    <th>Ações</th>
                                                </head>
                                                <?php $i = 1 ?>
                                                <?php foreach ($produtor->lotes as $lote): ?>
                                                    <tr>
                                                        <td><?= $i ?></td>
                                                        <td><?= $lote->nucleo ?></td>
                                                        <td><?= $lote->endereco ?></td>
                                                        <td><?= $lote->area ?></td>
                                                        <td>
                                                            <input type="hidden" class="jq_cpf"
                                                                   value="<?= $produtor->cpf ?>">
                                                            <a href="" class="jq_delete_lote" cod="<?= $lote->id ?>"><i
                                                                        class="fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        </div>
                                    </td>
                                    <td></td>
                                <?php endif ?>
                            </tr>
                            <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

