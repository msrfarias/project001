<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 23/02/2018
 * Time: 13:19
 */

class Restrict extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->auth();
    }

    protected function auth(){
        if (empty($this->session->nome)
            || !$this->session->logado
            ||  $this->session->key !== "CkvfG7s7blGj8HGkGzk79HOTiHQOttk2")

                header('Location: '.URL_PUBLIC."/login");
    }

}