<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    
</head>
<body>
<div id="recibo" >
    <div style="margin-right:auto; margin-left:auto;" >
        <p ><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span style="text-decoration: underline;">Recibo de Pagamento</span></strong></p>
        <p>Data: <?=date("d-m-Y",strtotime($compra->data))?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Recibo de Pagamento nº: <span id="idRecibo"><?=$compra->id?></span></p>
        <p>Nome: <?= $compra->produtor->nome?></p>
        <p>CPF:  <?= $compra->produtor->cpf?></p>
        <br><br>
        <table style="width:70%" id="tabelaItens">
            <head id="headItens">
                <th >Tipo</th>
                <th >Caixas</th>
                <th >Peso (Kg) </th>
                <th >Cotação (R$)</th>
                <th >Total (R$)</th>
            </head>
            <?php foreach ($compra->itens as $item):?>
            <tr id="vrd">
                <td height="40" ><?=strtoupper($item->tipo)?></td>
                <td height="40" ><?=$item->caixas?></td>
                <td height="40" ><?=$item->peso?></td>
                <td height="40" ><?=$item->cotacao?></td>
                <td height="40" ><?=$item->total_peso?></td>
            </tr>
            <?php endforeach;?>
        </table>
        <br><br>
        <p>Total da compra: R$ <?= number_format($compra->total->valor,2,","," ")?></p>
        <?php if (!empty($descontos)): ?>
        <?php $descontoTotal = 0?>
            Descontos:
            <table style="width:70%">
                <head>
                    <th >nº</th>
                    <th >Data</th>
                    <th >Valor</th>
                    <th >Desconto</th>
                    <th >Novo Restante</th>
                </head>
                <?php foreach ($descontos as $desconto):?>
                    <?php  $adiantamento = $desconto->adiantamento?>
                    <tr id="vrd">
                        <td height="40" ><?=$adiantamento->id?></td>
                        <td height="40" ><?= date("d-m-Y",strtotime($adiantamento->data))?></td>
                        <td height="40" >R$ <?= number_format($adiantamento->valor,2,","," ")?></td>
                        <td height="40" >R$ <?=number_format($desconto->valor,2,","," ")?></td>
                        <td height="40" >R$ <?= number_format($adiantamento->restante,2,","," ")?></td>
                    </tr>
                    <?php
                        $compra->total->valor -= $desconto->valor;
                        $descontoTotal += $desconto->valor;
                    ?>
                <?php endforeach;?>
            </table>
        <?php endif;?>
        <?php if (isset($descontoTotal)):?>
        <p>Desconto total: R$ <?= number_format($descontoTotal,2,","," ")?></p>
        <?php endif;?>
        <p>Foi pago a soma de R$ <span id="totalRecibo"><?=number_format($compra->total->valor,2,","," " )?></span>,( <span id="extensoRecibo"></span> ) como forma de <br>pagamento do intem descrito abaixo.</p>
        <p><strong>Forma de pagamento:&nbsp; </strong>(_) Dinheiro&nbsp;&nbsp; (_) Cheque (_) Transferência</p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;______________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__________________________<br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Assinatura</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Assinatura do Recebedor</strong></p>

    </div>
</div>

</html>