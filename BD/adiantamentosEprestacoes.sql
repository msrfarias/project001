-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09-Fev-2018 às 17:13
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `facerola`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `adiantamentos`
--

CREATE TABLE `adiantamentos` (
  `id` int(11) NOT NULL,
  `valor` decimal(10,2) UNSIGNED DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `pessoa_cpf` varchar(15) NOT NULL,
  `restante` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prestacoes`
--

CREATE TABLE `prestacoes` (
  `id` int(11) NOT NULL,
  `valor` decimal(10,2) UNSIGNED DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `adiantamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adiantamentos`
--
ALTER TABLE `adiantamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_adiantamentos_pessoa1_idx` (`pessoa_cpf`);

--
-- Indexes for table `prestacoes`
--
ALTER TABLE `prestacoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_prestacoes_adiantamentos1_idx` (`adiantamento_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adiantamentos`
--
ALTER TABLE `adiantamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `adiantamentos`
--
ALTER TABLE `adiantamentos`
  ADD CONSTRAINT `fk_adiantamentos_pessoa1` FOREIGN KEY (`pessoa_cpf`) REFERENCES `pessoas` (`cpf`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `prestacoes`
--
ALTER TABLE `prestacoes`
  ADD CONSTRAINT `fk_prestacoes_adiantamento` FOREIGN KEY (`adiantamento_id`) REFERENCES `adiantamentos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
