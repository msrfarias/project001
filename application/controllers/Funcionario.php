<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 27/01/2018
 * Time: 00:05
 */

require_once "Restrict.php";

class Funcionario extends Restrict {

    public function index(){
        $this->load->model("funcionarios");
        $tabela = $this->funcionarios->getAll();
        $this->load->view("cabecalho");
        $this->load->view("tabelaFuncionario",["funcionarios" => $tabela]);
        $this->load->view("modal_confirm",["pessoa"=>"funcionario"]);
        $this->load->view("rodape");
    }

    public function cadastro(){

        if (!isset($_POST["cadastro"])){
            $this->load->view("cabecalho");
            $this->load->view("funcionarios");
            $this->load->view("rodape");
            return;
        }
        if (
            !empty($_POST["nome"]) &&
            !empty($_POST["cpf"])
        ){
            $this->load->model("funcionarios");
           // $this->funcionarios = new Funcionarios();
            $this->funcionarios->setCpf($_POST["cpf"]);
            $this->funcionarios->setNome($_POST["nome"]);
            $this->funcionarios->setTelefone($_POST["telefone"]);
            $this->funcionarios->setEndereco($_POST["endereco"]);
            if (!empty($_POST['adm']))
                $this->funcionarios->setAsADM();
            if ($this->funcionarios->save()){
                http_response_code(200);
                echo "Cadastrado com sucesso";
                return;
            }
            http_response_code(500);
            echo "Este funcionario já está cadastrado";
            return;
        }
        http_response_code(406);
        echo "Dados insuficientes para cadastro";
        return;
    }

    public function remover(){
        if (empty($_POST["cpf"])){
            http_response_code(500);
            echo "Nao foi possivel atender esta requisição";
            return;
        }
        $this->load->model("funcionarios");
        $this->funcionarios->setCPF($_POST["cpf"]);
        if ($this->funcionarios->delete()){
            http_response_code(200);
            echo "Funcionário deletado com sucesso";
            return;
        }
        http_response_code(500);
        echo "Houve um erro inesperado ao tentar excluir";
    }
}

