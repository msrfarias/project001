<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div id="success"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Tabela de Pagamentos <small></small></h2>
                    <ul class="nav navbar-left panel_toolbox">
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="search" id="search" class="form-control" placeholder="Buscar" style="width:200px; ">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Buscar</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="add-on" href="<?=URL_PUBLIC?>/produtores/cadastro"><i class="fa fa-plus"></i></a>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">



                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action table-hover table-condensed" id="table">
                            <thead >
                                <tr class="headings">
                                    <th class="column-title">Produtor</th>
                                    <th class="column-title">Data</th>
                                    <th class="column-title">Preço</th>
                                    <th class="column-title">Desconto</th>
                                    <th class="column-title">Total</th>
                                    <th class="column-title no-link last"><span class="nobr"></span>
                                </tr>
                                
                            </thead>
                            <tbody>
                                <?php foreach ($todas as $compra):?>
                                <tr class="linha">
                                <td><?= $compra->produtor->nome?>&nbsp&nbsp&nbsp</td>
                                <td><?= date('d-m-Y',  strtotime($compra->data))?>&nbsp&nbsp&nbsp</td>
                                <td><?= number_format($compra->total->verde->peso + $compra->total->madura->peso,2,","," ")?></td>
                                <td><?= empty($compra->desconto)?"0,00":number_format($compra->desconto,2,","," ")?></td>
                                <td><?= number_format($compra->totalTudo,2,","," ")?></td>
                                <td class=" "><a href="#" class="btnRecibo" idc="<?=$compra->id?>"><i class="success fa fa-file-text" style="margin-left:40%"></i></a></td>
                                </tr>
                            <?php endforeach;?> 
                            </tbody>
                            

                        </table>
                        <div id="modal_recibo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Recibo de Pagamento <strong><span id="nome_delete"></span></strong></h4>
                                    </div>
                                    <form id="confirm_delete" action="">
                                        <div class="modal-body">
                                            <div class="panel panel-default">
                                                <div class="panel-body" id="recibo-modal">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button id="gerarCompra" class="btn btn-primary">Gerar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
