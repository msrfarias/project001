<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Nova Coleta<small>      </small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link" href="<?= empty($app)?"":URL_PUBLIC."/logout/app"?> "><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="formcoleta" class="form-horizontal form-label-left ">
                            <input type="hidden" name="coletaNova" value="true">
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" >
                                <?php $c = count($funcionarios) ?>
                                <select class="form-control" id="exampleSelect1" name="funcionario" <?= $c==1?"readonly value":""?>>
                                    <?php if( $c > 1):?>
                                    <option disabled selected>Selecione o Funcionário</option>
                                    <?php endif;?>
                                    <?php foreach ($funcionarios as $funcionario):?>
                                        <option value="<?=$funcionario->cpf?>" <?= $c==1?"selected":""?>><?=$funcionario->nome?></option>
                                    <?php  endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" >
                                <select class="form-control" id="exampleSelect1" name="produtor">
                                    <option disabled selected>Selecione o Produtor</option>
                                    <?php foreach ($produtores as $produtor):?>
                                        <option value="<?=$produtor->cpf?>"><?=$produtor->nome?></option>
                                    <?php  endforeach; ?>
                                </select>
                            </div>
                            <fieldset class="col-md-6" style="margin-top: 0.5%; margin-right: 10%">
                                <legend>Coleta</legend>

                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" >
                                            <select class="form-control" id="tipo" name="tipo">
                                                <option disabled selected>Selecione o Tipo</option>
                                                <option value="Verde">Verde</option>
                                                <option value="Madura">vermelha</option>

                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-right" id="quantidade" placeholder="Quantidade" name="quantidade">
                                            <span class="fa  form-control-feedback right" aria-hidden="true">Cx</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control" id="peso" placeholder="Peso" name="peso">
                                            <span class="fa form-control-feedback right" aria-hidden="true">Kg</span>
                                        </div>

                                        <div id="">
                                            <button id="addcompra" type="button" class="btn btn-default btn-sm  " aria-label="Right Align">
                                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                        <div id="escondido">

                                        </div>
                                        <table class="table table-striped jambo_table bulk_action" id="table-compra">
                                            <thead>
                                            <tr class="headings">
                                                <th class="column-title">Tipo</th>
                                                <th class="column-title">Quantidade</th>
                                                <th class="column-title">Peso </th>
                                                <th class="column-title no-link last"><span class="nobr">Ação</span>
                                                </th>
                                                <th class="bulk-actions" colspan="7">
                                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                            </fieldset>
                            <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <a href="<?= empty($app)?URL_PUBLIC:""?>"><button type="button" class="btn btn-primary">Cancelar</button></a>
                                    <button class="btn btn-primary" type="reset">Limpar</button>
                                    <button type="submit" class="btn btn-success">Confirmar</button>
                                </div>
                            </div>
                            <div id="success"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>