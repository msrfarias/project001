<?php
/**
 * Created by PhpStorm.
 * User: IACF
 * Date: 07/02/2018
 * Time: 23:35
 */

require_once "Restrict.php";

class Adiantamento extends Restrict
{

    public function index(){
        $this->load->model("adiantamentos");
        $this->load->model("pessoas");
        $adiantamentos = $this->adiantamentos->get();
        foreach ($adiantamentos as $adiantamento){
            $adiantamento->produtor = $this->pessoas->get($adiantamento->pessoa_cpf);
            $adiantamento->parcelas = $this->adiantamentos->getParcelas($adiantamento->id);
        }

        $this->load->view("cabecalho");
        $this->load->view("tabelaAdiantamento", ["adiantamentos" => $adiantamentos]);
        $this->load->view("rodape");
    }

    public function cadastro(){

        if (empty($_POST["produtor"]) || empty($_POST["valor"])){
            $this->load->model("pessoas");
            $produtores = $this->pessoas->getAllProdutores();
            $this->load->view("cabecalho");
            $this->load->view("Novoadiantamento",["produtores" =>$produtores]);
            $this->load->view("rodape");
            return;
        }
        $this->load->model("adiantamentos");
        $this->adiantamentos->setPessoaCpf($_POST["produtor"]);
        $_POST["valor"] = str_replace(".","",$_POST['valor']);
        $_POST["valor"] = str_replace(",",".",$_POST['valor']);
        $this->adiantamentos->setValor($_POST["valor"]);
        if ($this->adiantamentos->save()){
            http_response_code(200);
            echo "Cadastrado com sucesso";
            return;
        }
        http_response_code(500);
        echo "Houve um erro ao realizar este cadastro";
    }

    public function produtor(){
        if (empty($_POST["cpf"])){
            http_response_code(406);
            echo "CPF não foi enviado";
        }
        $this->load->model("adiantamentos");
        $parcelas = $this->adiantamentos->getByCpf($_POST["cpf"]);
        http_response_code(200);
        echo json_encode($parcelas);
    }

    public function recibo(){
        if (empty($_POST["adiantamento"])){
            if (empty($_POST["valor"])){
                http_response_code("500");
                echo "Informações não foram enviadas corretamente";
            }
            $data = date("d-m-Y",time());
            $valor = $_POST["valor"];
            $adiantamento = new stdClass();
            $adiantamento->data = $data;
            $adiantamento->valor = $valor;
            $adiantamento->pessoa_cpf = $_POST["produtor_cpf"];
            $this->load->model("adiantamentos");
            $adiantamento->id = $this->adiantamentos->getLastId() + 1;
        }
        else{
            $this->load->model("adiantamentos");
            $adiantamento = $this->adiantamentos->get($_POST['adiantamento']);
            if (count($adiantamento) == 0){
                http_response_code(404);
                echo "Este recibo não foi encontrado";
            }
            $adiantamento = $adiantamento[0];
        }

        $this->load->model("pessoas");

        $produtor = $this->pessoas->get($adiantamento->pessoa_cpf);
        $vars = [
            "adiantamento" => $adiantamento,
            "produtor"     => $produtor
        ];
        $this->load->view("reciboAdiantamento",$vars);
    }
}


