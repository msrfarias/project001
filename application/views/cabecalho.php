<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FA - Fernando Acerola </title>

    <!-- Bootstrap -->
    <link href="<?=URL_PUBLIC?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?=URL_PUBLIC?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <!-- iCheck -->

    <!-- bootstrap-progressbar -->
    <link href="<?=URL_PUBLIC?>/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
<!--    <link href="--><?//=URL_PUBLIC?><!--/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>-->
    <!-- bootstrap-daterangepicker -->
    <link href="<?=URL_PUBLIC?>/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=URL_PUBLIC?>/build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=URL_PUBLIC?>/css/estilo.css">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index.php" class="site_title">

                        </i> <span>Fernando Acerola</span></a>

                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="<?=URL_PUBLIC?>/images/logo2.jpg" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Bem Vindo,</span>
                        <h2><?= explode(" ",trim($this->session->nome))[0]?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Geral</h3>
                        <ul class="nav side-menu">
                            <li><a href="<?=URL_PUBLIC?>"><i class="fa fa-home"></i> Inicio <span class="fa "></span></a>

                            </li>
                            <li><a><i class="fa fa-edit"></i> Cadastros<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">

                                    <li><a type="button" class="" data-toggle="modal" data-target=".bs-example-modal-sm">Cotação da Semana</a></li>
                                    <li><a href="<?= URL_PUBLIC."/produtores/cadastro"?>">Produtores</a></li>
                                    <li><a href="<?= URL_PUBLIC."/Funcionario/cadastro"?>">Funcionários</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-desktop"></i> Coletar <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?= URL_PUBLIC."/coleta/nova"?>">Nova coleta</a></li>
                                    <li><a href="<?=URL_PUBLIC ?>/coleta/listar">coletado</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-desktop"></i> Pagamento <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                        <li><a href="<?=URL_PUBLIC ?>/compra/pagar">Pagar</a></li>
                                </ul>
                            </li>
                            <li><a href="<?= URL_PUBLIC."/Adiantamento/cadastro"?>"><i class="fa fa-money"></i> Adiantar <span class="fa"></span></a>

                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <h3>Administrador</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-building-o"></i> Relatórios <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="<?=URL_PUBLIC."/produtores"?>">Produtores</a></li>
                                    <li><a href="<?=URL_PUBLIC."/Adiantamento"?>">Adiantamentos</a></li>
                                    <li><a href="<?=URL_PUBLIC."/compra/pagos"?>">Pagamentos</a></li>
                                    <li><a href="<?=URL_PUBLIC."/funcionario"?>">Funcionários</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?=URL_PUBLIC."/logout"?>" title="Sair">Sair<i class="fa fa-sign-out pull-right"></i></a></li>
                    </ul>
                </nav>

                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Cotação</h4>
                            </div>
                            <form id="frmCotacoes">
                            <div class="modal-body">



                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="col-md-8 col-sm-6 col-xs-12 form-group has-feedback">
                                                <input type="text" name="valor" class="form-control money" id="inputSuccess5" placeholder="Valor" required="required" >
                                                <span class="fa fa-usd form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                            <div class="col-md-8 col-sm-6 col-xs-14 form-group has-feedback" >
                                                <select class="form-control" name="tipo" id="exampleSelect1">
                                                    <option value="0">tipo</option>
                                                    <option value="VERDE">Verde</option>
                                                    <option value="MADURA">Vermelha</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="succsess"></div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary">Salvar alterações</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div id="res"></div>
