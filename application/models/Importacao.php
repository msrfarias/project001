<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 18/02/2018
 * Time: 14:57
 */

class Importacao extends CI_Model
{

    public function importar(){
        $this->load->database();
        $this->db->from("pessoas");
        $pessoas = $this->db->get()->result();
        foreach ($pessoas as $pessoa){
            $cpf = preg_replace("/[^0-9]/","",$pessoa->cpf);
            $cpf = [substr($cpf,0,3),substr($cpf,3,3),substr($cpf,6,3),substr($cpf,9,2)];
            $cpf = "$cpf[0].$cpf[1].$cpf[2]-$cpf[3]";

            $nova_pessoa = [
                "cpf"        => "$cpf",
                "nome"       => "$pessoa->nome",
                "telefone"   => "$pessoa->telefone",
                "produtor"   => "$pessoa->produtor",
                "observacao" => "$pessoa->observacao",
                "email"      => "$pessoa->email"
            ];
            $this->db->insert(_BD_."pessoas",$nova_pessoa);
            $conta_pessoa = [
                "pessoa"     => "$cpf",
                "banco"      => "$pessoa->banco",
                "agencia"    => "$pessoa->agencia",
                "conta"      => "$pessoa->conta"
            ];
            $this->db->insert(_BD_."contas",$conta_pessoa);
            $lotes = $this->db->get_where("lotes",["dono_cpf"=>"$pessoa->cpf"])->result();
            foreach ($lotes as $lote){
                $novo_lote = [
                    "nucleo"  => "$lote->nucleo",
                    "lote"    => "$lote->lote",
                    "endereco"=> "$lote->endereco",
                    "area"    => "$lote->area",
                    "dono_cpf"=> "$cpf"
                ];
                $this->db->insert(_BD_."lotes",$novo_lote);
            }
        }
    }


}