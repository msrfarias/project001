<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 18/12/2017
 * Time: 10:55
 */

require_once "Bean.php";

class Lotes  extends  CI_Model implements Bean
{

    private $id;
    private $nucleo;
    private $lote;
    private $endereco;
    private $area;
    private $dono_cpf;

    function save(){
        $this->load->database();
        $data = [
            "nucleo"  => $this->getNucleo(),
            "lote"    => $this->getLote(),
            "endereco"=> $this->getEndereco(),
            "area"    => $this->getArea(),
            "dono_cpf"=> $this->getDonoCpf()
        ];

        $this->db->insert(_BD_."lotes",$data);
    }

    function get($cpf)
    {
        $this->load->database();
        $query = $this->db->get_where(_BD_."lotes",["dono_cpf"=>$cpf]);
        $result = $query->result();
        $res = [];
        @$keys = get_object_vars($result[0]);
        foreach ($result as $row) {
            $obj = new stdClass();
            foreach ($keys as $key => $val) {
                $obj->$key = $row->$key;
            }
            $res[] = $obj;
        }
        return $res;
    }

    function delete($cpf,$id=0)
    {
        $this->load->database();
        $dados = [
            "dono_cpf" => $cpf
            ];

        if ($id > 0){
            $dados["id"] = $id;
        }
        $this->db->delete(_BD_.'lotes',$dados);

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNucleo()
    {
        return $this->nucleo;
    }

    /**
     * @param mixed $nucleo
     */
    public function setNucleo($nucleo)
    {
        $this->nucleo = $nucleo;
    }

    /**
     * @return mixed
     */
    public function getLote()
    {
        return $this->lote;
    }

    /**
     * @param mixed $lote
     */
    public function setLote($lote)
    {
        $this->lote = $lote;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }

    /**
     * @return mixed
     */
    public function getDonoCpf()
    {
        return $this->dono_cpf;
    }

    /**
     * @param mixed $dono_cof
     */
    public function setDonoCpf($dono_cpf)
    {
        $this->dono_cpf = $dono_cpf;
    }



}