<div id="modal_confirm" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Excluir <strong><span class="nome_delete"></span></strong>?</h4>
            </div>
            <form id="confirm_delete" action="<?=URL_PUBLIC?>/<?=$pessoa?>/remover">
                <input type="hidden" name="cpf">
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <span>Confirma a exclusao de <strong><span class="nome_delete"></span></strong>?</span>
                            <span><strong>Cuidado!</strong>Esta ação não poderá ser desfeita,</span>
                                <br>
                            <strong>Todas</strong> as coletas em que <strong><span class="nome_delete"></span></strong> esteve envolvido
                                serão perdidas permanentemente.

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Excluir</button>
                </div>
            </form>
        </div>
    </div>
</div>