function alertt(data,success) {
    if (success){
        $('#success').html("<div class='alert alert-success'>");
        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
            .append("</button>");
        $('#success > .alert-success')
            .append("<strong>"+data+"</strong>");
        $('#success > .alert-success')
            .append('</div>');
        return;
    }
    $('#success').html("<div class='alert alert-danger'>");
    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
        .append("</button>");
    $('#success > .alert-danger').append("Houve um erro ao realizar esta operação: "+data);
    $('#success > .alert-danger').append('</div>');

}
$(function() {

    $("#formProdutor").submit(function (event) {
        event.preventDefault();
        dados = $(this).serialize();
        //console.log(dados);
        var action = $(this).attr("action");
        $.ajax({
            url: "http://"+location.hostname+"/produtores/"+action,
            type : "POST",
            cache : false,
            data : dados,
            success : function(data){
                console.log(data);
                //clear all fields
                $("#escondidos").html("");
                alertt(data, true);
                if (action === "cadastroJa") {
                    $('#formProdutor').trigger("reset");
                    $("#table-lotes").children("tbody").children("tr").remove();
                }
                //$('#formProdutor').trigger("reset");
            },
            error : function (data) {
                console.log(data);
                alertt(data.responseText,false);
            }

        });
    });
    
});


$("#formFuncionario").submit(function (event) {
    event.preventDefault();
    var dados = $(this).serialize();
    $.ajax({
        url: "http://"+location.hostname+"/funcionario/cadastro",
        method: "POST",
        data: dados,
        success: function (data) {
            alertt(data, true);
            $("#formFuncionario").trigger('reset');
        },
        error:function (data) {
            alertt(data.responseText,false);
        }
    });

});

$("#formcoleta").submit(function(event){
    event.preventDefault();
    var dados = $(this).serialize();
    $.ajax({
        url:"http://"+location.hostname+"/coleta/nova",
        method:"POST",
        cache : false,
        data: dados,
        success:function(data){
            alertt(data,true);
            $("#formcoleta").trigger('reset');
            $("#table-compra").children('tbody').html("");
            $("#escondido").html("");
        },
        error:function(data){
            alertt(data.responseText,false);
        }
    });
});
$("#formadiantamento").submit(function (event) {
   event.preventDefault();
    $("#table-adiantamento").children('tbody').html("");
   var dados = $(this).serialize();
   $.ajax({
       url: "http://"+location.hostname+"/adiantamento/cadastro",
       method:"POST",
       data: dados,
       success:function (data) {
           alertt(data,true);
           $("#formadiantamento").trigger('reset');


       },
       error:function (data) {
           alertt(data.responseText, false);
       }
   });
   $("#reciboA").load("http://"+location.hostname+"/adiantamento/recibo",{"valor":$("#valor").val(),"produtor_cpf":$("#cpfadiantamento").val()});
});



$("#formdesconto").submit(function (event) {
    event.preventDefault();
    var des = $("#valor_desc").val();
    //$("#desconto").val(des);
    des = parseFloat(des);
    var val =  parseFloat($("#restante").val().replace("R$",""));
    var el = $("span[selecionado]");
    var apl = el.parents("tr:eq(0)").find(".aplyDesconto");
    var gVal = parseFloat((apl.html().replace("R$","")).replace(",","."));
    var res = val - des + gVal;
    var aplV = des.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}).replace("R$","R$ ");
    apl.html(aplV);
    if (des > 0)
        el.parents("tr:eq(0)").attr("apl",des);
    if (res < 0){
        el.parents("tr:eq(0)").removeAttr("apl");
        el.addClass("valorErrado");
    }
    res = res.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}).replace("R$","R$ ");
    el.html(res);
    $("#modal_adiantamento").modal('hide');
    el.removeAttr("selecionado");
});

$(".jq_adiantamento").click(function (event) {
    event.preventDefault();
   // $("#id_adiantamento").val($(this).attr("codigo"));
    var linha =  $(this).parents("tr:eq(0)");
    linha.attr("apl");
    var td = linha.find(".restante_td:eq(0)");
    td.attr("selecionado","");
    $("#restante").val(td.html().replace("R$",""));
    //var val = parseFloat($(this).parents("tr:eq(0)").attr("des"));
    //alert(val);
    $("#modal_adiantamento").modal();
});

$(".selectProdutor").change(function (event) {

   var dado = $(this).val();
   $("#cpfadiantamento").val(dado);
    $("#table-adiantamento").children('tbody').html("");
   $.ajax({
       url:"http://"+location.hostname+"/adiantamento/produtor",
       method: "POST",
       data:{
         "cpf":dado
       },
       success:function (data) {

           data = JSON.parse(data);
           console.log(data);
           for(var i = data.length-1; i >= 0;i--)
           {

               AddTableRow = function(i) {

                   var newRow = $("<tr id='linha"+i+"'>");
                   var cols = "";

                   cols += "<td>"+moment(data[i].data).format('DD-MM-YYYY')+"</td>";
                   cols += "<td>"+data[i].restante+"</td>";

                   newRow.append(cols);
                   $("#table-adiantamento").append(newRow);

               };
               AddTableRow(i);
           }

       },
       error:function (data) {
           console.log(data);
       }
   });
});
var i=1;


function removeTableRow(i) {
    $("#linha"+i).remove();
    $("#tipo"+i).remove();
    $("#quantidade"+i).remove();
    $("#peso"+i).remove();
    $("#nucleo"+i).remove();
    $("#endereco"+i).remove();  
    $("#area"+i).remove();
}


$(".linhaC").click(function () {
    $(this).closest("tr").next().toggleClass("linhaEscondida");
});

$("#add").click(function t() {

    var nc = $("#nucleo").val();
    var end = $("#endereco").val();
    var area = $("#area").val();

    $("#escondidos").html($("#escondidos").html()+"<input id='nucleo"+i+"' type='hidden' name='nucleo[]' value='"+nc+"'>");
    $("#escondidos").html($("#escondidos").html()+"<input id='endereco"+i+"' type='hidden' name='endereco[]' value='"+end+"'>");
    $("#escondidos").html($("#escondidos").html()+"<input id='area"+i+"' type='hidden' name='area[]' value='"+area+"'>");
    AddTableRow = function(cont) {

        var newRow = $("<tr id='linha"+cont+"'>");
        var cols = "";

        cols += "<td>"+nc+"</td>";
        cols += "<td>"+end+"</td>";
        cols += "<td>"+area+"</td>";
        cols += '<td><a href="#" id="jq_remove'+cont+'" onclick="removeTableRow('+cont+');preventDefault()"><i class="success fa fa-trash"></i></a>';
        cols += '</td>';

        newRow.append(cols);
        $("#table-lotes").append(newRow);

    };

    AddTableRow(i);
    document.getElementById("jq_remove"+(i)).addEventListener("click", function(event){
        event.preventDefault();});
    i++;
// exibindo o valor em alert
    $("#nucleo").val("Selecione Seu nucleo");
    $("#endereco").val("");
    $("#area").val("");

});

$("#frmCotacoes").submit(function (ev) {
    ev.preventDefault();
    var action = $(this).attr("action");
    var dados = $(this).serialize();
    var self = $(this);
    $.ajax({
        url : "http://"+location.hostname+"/cotacao/cadastro",
        cache : false,
        method : "POST",
        data : dados,
        success : function (data) {
            var res = JSON.parse(data);
            id = "cotacao"+res.tipo;
            $("#"+id).html(res.valor);
            self.parents(".modal:eq(0)").modal("hide");
            init_flot_chart();
        },
        error   : function (data) {
            alertt(data.responseText,false);
            console.log(data);

        }
    });
});


$(".jq_delete_lote").click(function (event) {
   var cpf = $(this).parent().find(".jq_cpf").val();//$(this).parent().parent().parent().parent().parent().parent().parent().find(".jq_cpf").html();
    event.preventDefault();
    var id = $(this).attr("cod");
    var self = $(this);
   $.ajax({
       url: "http://"+location.hostname+"/lote/excluir",
       type : "POST",
       cache : false,
       data : {
           "cpf": cpf,
           "id": id
       },
       success : function(data){
           console.log(data);
           self.parents("tr:eq(0)").hide();
           alertt(data,true);
       },
       error   : function (data) {
           alertt(data.responseText,false);
       }
   });
});




$(".jq_delete").click(function (event) {
    event.preventDefault();
    var nome = $(this).closest("tr").find(".jq_nome").html();

    $("#confirm_delete").children("input[name='cpf']").val($(this).attr("cpf"));

    var nomes = $(".nome_delete");
    console.log(nomes);
    nomes.each(function(){
        $(this).html(nome);
    });
    $("#modal_confirm").modal();
});

$("#confirm_delete").submit(function (ev) {
    ev.preventDefault();
    var self = $(this);
    // alert(self.attr("action"));
    $.post( self.attr("action"),
            self.serialize()
    ).done( function (data) {
        alertt(data,true);
        var busca = ".jq_delete[cpf=\"";
        busca += self.children("input[name='cpf']").val();
        busca += "\"]";
        var linha = $(busca).parents("tr:eq(0)");
        if (linha.hasClass("linhaC"))
            linha.next("tr").hide();
        linha.hide();
    }).error(function (data) {
        alertt(data.responseText,false);
        console.log(data);
    }).always(function () {
        $("#modal_confirm").modal("hide");
    });
});


$("#addcompra").click(function tc() {

    var tipo = $("#tipo").val();
    var quantidade = $("#quantidade").val();
    var peso = $("#peso").val();    

    $("#escondido").html($("#escondido").html()+"<input id='tipo"+i+"' type='hidden' name='tipo[]' value='"+tipo+"'>");
    $("#escondido").html($("#escondido").html()+"<input id='quantidade"+i+"' type='hidden' name='quantidade[]' value='"+quantidade+"'>");
    $("#escondido").html($("#escondido").html()+"<input id='peso"+i+"' type='hidden' name='peso[]' value='"+peso+"'>");
    AddTableline = function(cont) {

        var newRow = $("<tr id='linha"+cont+"'>");
        var cols = "";

        cols += "<td>"+tipo+"</td>";
        cols += "<td>"+quantidade+"</td>";
        cols += "<td>"+peso+"</td>";
        cols += '<td><a href="#" id="jq_remove'+cont+'" onclick="removeTableRow('+cont+');"><i class="success fa fa-trash"></i></a>';
        cols += '</td>';

        newRow.append(cols);
        $("#table-compra").append(newRow);

    };

    AddTableline(i);
    document.getElementById("jq_remove"+(i)).addEventListener("click", function(event){
        event.preventDefault();});
    i++;
// exibindo o valor em alert
    $("#tipo").prop('selectedIndex',0);
    $("#quantidade").val("");
    $("#peso").val("");

});

$("#formcompra").submit(function (event) {
        event.preventDefault();
        dadoscompra = $(this).serialize();
        console.log(dadoscompra);
        // alert(action);e
        // return;
        // alert("http://"+location.hostname+"/produtores/cadastroJa");
        $.ajax({
            url: "http://"+location.hostname+"/compra/nova",
            type : "POST",
            cache : false,
            data : dadoscompra,
            success : function(data){
                alertt(data,true);
                //clear all fields
                  $('#formcompra').trigger("reset");
                  $("#escondido").html("");
                  $("#table-compra").children("tbody").children("tr").remove();
            },
            error : function (data) {
                alertt(data.responseText,false);
                //clear all fields
                $('#formcompra').trigger("reset");
            }

        });
});

$(".jq_send").click(function (event) {
    event.preventDefault();
    $(this).children(".frm_confirm").submit();
});

$(".frm_confirm").submit(function (event) {
    event.preventDefault();
    var action = $(this).attr('action');
    var message = $(this).attr("message");
    var titulo = $(this).attr("titulo");
    var linha = $(this).parents("tr:eq(0)");
    var dados = $(this).serialize();
    console.log("auqi auqi : ");
    console.log(dados);
    // alert(action +"\n"+ message+"\n");

    $(".message_modal_confirm").html(message);
    $(".title_modal_confirm").html(titulo);
    $("#modal_confirm").modal();
    $(".modal_confirm_btn").click(function (e) {
        e.preventDefault();
        $.ajax({
            url : action,
            data : dados,
            cache : false,
            type: "POST",
            success: function(data){
                alert(data);
                linha.hide();
                if (linha.hasClass("linhaC")){
                    linha.next("tr").hide();
                }
                $("#modal_confirm").modal("hide");
                alertt(data,true);
            },
            error : function (data) {
                console.log(data);
                alertt(data.responseText,false);
            }
        });
    });
});

$(".jq_resetAdiantamento").click(function (e) { /// remover apl
    e.preventDefault();
    var s = $(this).parents("tr:eq(0)");
    var v = s.find(".aplyDesconto");
    var d = s.find(".restante_td");

    var v_value = v.html().replace(",",".").replace("R$","").replace(" ","");
    var d_value = d.html().replace(",",".").replace("R$","").replace(" ","");

    d.html("R$" + (parseFloat(v_value) + parseFloat(d_value)).toFixed(2));
    v.html("R$ 0,00");

    s.find(".valorErrado").removeClass("valorErrado");
    s.removeAttr("apl");

});


$(".clsMA").click(function () {

    target = $("span[selecionado]");
    target.removeAttr("selecionado");
    target.parents("tr:eq(0)").removeAttr("apl");
});

$(".frm_edit").submit(function (event) {
    event.preventDefault();
    dadoseditados = $(this).serialize();
    //console.log(dadoseditados);
    // alert(action +"\n"+ message+"\n");
    $.ajax({
        url : $(this).attr("action"),
        data : dadoseditados,
        cache : false,
        type: "POST",
        success: function(data){
            location.reload(true);
        },
        error : function (data) {
            console.log(data);
            alertt(data.responseText,false);
        }
    });
});





$(".jq_edit").click(function (event) {
    event.preventDefault();
    // alert("foi");
    // console.log($(this).children(".frm_confirm"));
    $("#jq_peso").val($(this).children(".jq_edit_peso").val());
    $("#jq_cotacao").val($(this).children(".jq_edit_cotacao").val());
    $("#modal_edit").modal();
    $(".frm_edit").attr("action", "http://"+location.hostname+"/compra/item/alterar/"+$(this).children(".jq_edit_id").val());


});

$(".jq_edit_coleta").click(function(event){
    event.preventDefault();
    var valor = $(this).attr("valor");
    $("#jq_peso_coleta").val($(this).children(".jq_edit_pesos").val());
    $("#jq_caixas").val($(this).children(".jq_edit_caixas").val());
    $("#jq_escondido").val(valor);
    $("#modal_edit_coleta").modal();
});
$("#frm_editColeta").submit(function(event){
    event.preventDefault();
    dados = $(this).serialize();
    $.ajax({
        url: "http://"+location.hostname+"/coleta/alteraItem",
        data: dados,
        type:"POST",
        success: function(data){
            location.reload(true);
        },
        error :function(data){
            alert("erro"+data.responseText);
        }
    });
});


$(document).ready(function(){
    $("#cpf").inputmask("999.999.999-99");  //static mask
    //$("#inputSuccess5").inputmask("99.99");

    $('#valor').inputmask('decimal', {
        radixPoint:",",
        groupSeparator: ".",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        placeholder: '0',
        rightAlign: false,
        onBeforeMask: function (value, opts) {
            return value;
        }
    });
    $('#inputSuccess5').inputmask('decimal', {
        radixPoint:",",
        groupSeparator: ".",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        placeholder: '0',
        rightAlign: false,
        onBeforeMask: function (value, opts) {
            return value;
        }
    });

    $('#jq_cotacao').inputmask('decimal', {
        radixPoint:",",
        groupSeparator: ".",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        placeholder: '0',
        rightAlign: false,
        onBeforeMask: function (value, opts) {
            return value;
        }
    });


    $('#valor_desc').inputmask('decimal', {
        radixPoint:",",
        groupSeparator: ".",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        placeholder: '0',
        rightAlign: false,
        onBeforeMask: function (value, opts) {
            return value;
        }
    });

    $('#jq_peso').inputmask('decimal', {
        radixPoint:".",
        groupSeparator: ".",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        placeholder: '0',
        rightAlign: false,
        onBeforeMask: function (value, opts) {
            return value;
        }
    });
    $('#jq_peso_coleta').inputmask('decimal', {
        radixPoint:".",
        groupSeparator: ".",
        autoGroup: true,
        digits: 2,
        digitsOptional: false,
        placeholder: '0',
        rightAlign: false,
        onBeforeMask: function (value, opts) {
            return value;
        }
    });

});


var j = 1;
function remove(i) {
    $("#line"+i).remove();
}
$("#addc").click(function tc() {


    function addConta(count){
        var newDiv = $("<div id='line"+count+"'></div>");
        var newExcluir = $("<button id='addm' type='button' c="+count+" class='btn btn-default btn-sm' aria-label='Right Align' onclick=remove("+count+");>");
        var excluir = "";
        var newBanco = $("<div class='col-md-6 col-sm-6 col-xs-12 form-group has-feedback teste' style='width: 32%;'>");
        var banco = "";
        var newAgencia = $("<div class='col-md-6 col-sm-6 col-xs-12 form-group has-feedback' style='width: 30%'>");
        var agencia = "";
        var newConta = $("<div class='col-md-6 col-sm-6 col-xs-12 form-group has-feedback' style='width: 30%'>");
        var conta = "";
        excluir += "<span class='glyphicon glyphicon-minus' aria-hidden='true'></span>";+"</button>";
        banco += "<input type='text' class='form-control has-feedback-left' id='banco' name='banco[]' placeholder='Banco' required='required'>"+"<span class='fa fa-map-marker form-control-feedback left' aria-hidden='true'></span>";
        banco += "</div>";
        agencia += "<input type='text' class='form-control' id='agencia' name='agencia[]' placeholder='Agencia' required='required'>"+"<span class='fa fa-money form-control-feedback right' aria-hidden='true'></span>";
        agencia += "</div>";
        conta += " <input type=\"text\" class='form-control' id='conta' name='conta[]' placeholder='Conta' required='required'>"+"<span class='fa fa-money form-control-feedback right' aria-hidden='true'></span>";
        conta += "</div>";
        newExcluir.append(excluir);
        newBanco.append(banco);
        newAgencia.append(agencia);
        newConta.append(conta);
        $("#form_conta").append(newDiv);
        $("#line"+j).append(newExcluir)
            .append(newBanco)
            .append(newAgencia)
            .append(newConta);

    }
    addConta(j);
    j++;
});


function somaOsSelecionadosEmPagar() {
    var soma = 0.00;
    var strSoma = "";
    $(".linhaC:visible").each(function () {
        var v = $(this).find("td.jq_valorCompra").html();
        v = v.replace(",",".").replace(' ',"").replace("R$","");
        v = v.replace(" ","");
        strSoma += " "+v+" + ";
        soma += parseFloat(v);
    });
    strSoma = soma.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}).replace("R$","R$ ");
    $("#totalSelecionados").html("( "+ strSoma+ " )");
}


$("#search").keypress(function(){
    somaOsSelecionadosEmPagar();
    $(".hiddenRow").each(function () {
        $(this).addClass("linhaEscondida");
        $(this).find(".accordion-body").removeClass("in");
    });
}).change(function(){somaOsSelecionadosEmPagar()});


$(function () {
    $( '.table' ).searchable({
        striped: true,
        selector:".linha",
        searchType: 'strict'
    })
});

$('.money').mask('000.000.000.000.000,00', {reverse: true});