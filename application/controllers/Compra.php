<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 21/12/2017
 * Time: 09:08
 */

require_once "Restrict.php";

class Compra extends Restrict
{

    public function nova(){

        if (empty($_POST["produtor"])){
            $this->load->model("pessoas");
            $produtores = $this->pessoas->getAllProdutores();
            $this->load->view("cabecalho");
            $this->load->view("novacompra",["produtores" => $produtores]);
            $this->load->view("rodape");
            return;
        }
        $produtor = $_POST['produtor'];
        $this->load->model("compras");
        $this->compras->setPessoaCpf($produtor);
        $this->compras->setData(date("Y-m-d",time()));
        //$this->compras->setStatus('AGUARDO');
        $this->compras->save();
        $this->compras->validarColeta();
        $this->load->model("itensCompra");
        $this->load->model("cotacoes");

        for ($i=0; $i< count($_POST["tipo"]); $i++){
            $this->itensCompra->setTipo($_POST['tipo'][$i]);
            $this->itensCompra->setCaixas(empty($_POST['quantidade'][$i])?0:$_POST['quantidade'][$i]);
            $this->itensCompra->setPeso(empty($_POST['peso'][$i])?0:$_POST['peso'][$i]);
            $this->itensCompra->setCompraId($this->compras->getId());
            $cot = $this->cotacoes->getUltimaVAlida($_POST['tipo'][$i]);
            $this->itensCompra->setCotacao($cot->valor);
            $this->itensCompra->save();
        }
    }


    private function excluir_item(){

        $this->load->model("itensCompra");
        $this->itensCompra = new ItensCompra();
        $id = empty($_POST["id_item"])?0:$_POST["id_item"];
        if (!$id) {
            echo "nao excluiu";
            return;
        }
        $this->itensCompra->delete($id);
        echo "excluiu";
    }

    private function alterar_item($id){
        $cot = empty($_POST["cotacao"])?0:$_POST["cotacao"];
        $peso = empty($_POST["peso"])?0:$_POST["peso"];

        $this->load->model("itensCompra");
//        $this->itensCompra = new ItensCompra();
        $this->itensCompra->setCotacao($cot);
        $this->itensCompra->setPeso($peso);
        $this->itensCompra->update($id);
    }

    public function item($action,$id=0){
        switch ($action){
            case "excluir" : return $this->excluir_item();
            case "alterar" : return $this->alterar_item($id);
        }
    }

    public function remover(){
        if (empty($_POST['id_compra']))
            return;
        $id = $_POST['id_compra'];
        $this->load->model("compras");
//        $this->compras = new Compras();
        $this->compras->delete($id);
    }

    public function pagar(){

        if (!empty($_POST["id_compra"])){
            $this->load->model("compras");
            $this->load->model("adiantamentos");
            $this->adiantamentos = new Adiantamentos();
            $this->compras->setId($_POST["id_compra"]);
            if (!empty($_POST["descontos"])){
                $descontos = $_POST["descontos"];
                $adiantamentos = $_POST["adiantamentos"];
                for($i=0;$i<count($descontos);$i++){
                    $this->adiantamentos->setId($adiantamentos[$i]);
                    if($this->adiantamentos->addParcela($_POST["id_compra"],$descontos[$i])==false ){
                        http_response_code(500);
                        echo "Erro ao aplicar desconto. Este valor de desconto não pode ser aplicado.";
                        echo "\nad: ".$adiantamentos[$i];
                        echo "\ndesc: ".$descontos[$i];
                        return;
                    }
                }
            }
            $this->compras->pagar();
            http_response_code(200);
            echo "Pagamento efetuado com sucesso";
            return;
        }
        $this->load->model("compras");
        $this->load->model("itensCompra");
        $this->load->model("pessoas");
        $this->load->model("adiantamentos");
//        $this->adiantamentos = new Adiantamentos();


        $todas = $this->compras->getCompras();
        foreach($todas as $compra){
            $compra->produtor = $this->pessoas->get($compra->pessoa_cpf);
            $compra->itens = $this->itensCompra->get($compra->id);
            @$compra->total->verde = $this->compras->get_totais($compra->id,'VERDE');
            @$compra->total->madura = $this->compras->get_totais($compra->id,'MADURA');
            $compra->produtor->adiantamentos = $this->adiantamentos->getByCpf($compra->pessoa_cpf);
        }

        $this->load->view("cabecalho");
        $this->load->view("pagar",["compras"=>$todas,"produtores" => $this->pessoas->getAllProdutores()]);
        $this->load->view("modalConfirm");
        $this->load->view("rodape");

    }

    public function recibo(){

        if (empty($_POST["id_compra"])){
            echo "Impossivel imprimir este recibo";
            return;
        }
        $id = $_POST["id_compra"];
        $this->load->model("compras");
        $this->load->model("itensCompra");
        $this->load->model("pessoas");
        $this->load->model("adiantamentos");
        $this->adiantamentos = new Adiantamentos();
        $compra = $this->compras->getCompra($id);
        $this->compras->setId($id);

        $compra = $compra[0];
        $compra->produtor = $this->pessoas->get($compra->pessoa_cpf);
        $compra->itens = $this->itensCompra->get($compra->id);
        @$compra->total->verde = $this->compras->get_totais($compra->id,'VERDE');
        @$compra->total->madura = $this->compras->get_totais($compra->id,'MADURA');
        $compra->total->valor = $compra->total->verde->peso;
        $compra->total->valor += $compra->total->madura->peso;

        $compra->data = date("d/m/Y",strtotime($compra->data));
        $desconto = empty($_POST["descontos"])?[]:$_POST["descontos"];
        $adiantamento = empty($_POST["adiantamentos"])?[]:$_POST["adiantamentos"];
        $descontos = [];
        $r =false;
        if (empty($desconto))
            $r = true;
            foreach ($this->compras->getDescontos() as $desc) {
                $adiantamento[] = $desc->adiantamento_id;
                $desconto[] = $desc->valor;
            }

        for ($i = 0; $i < count($desconto); $i++) {
            $descontos[$i] = new stdClass();
            $descontos[$i]->valor = $desconto[$i];
            $descontos[$i]->adiantamento = $this->adiantamentos->get($adiantamento[$i])[0];
            $descontos[$i]->adiantamento->restante -= $r?0:$desconto[$i];
        }
        $v = [
            "compra"       => $compra,
            "descontos"     => $descontos
        ];

//        echo "<pre>";
//        var_dump($_POST);
//        echo "</pre>";

        $this->load->view("recibo",$v);
    }

    public function pagos(){

        $this->load->model("compras");
        $this->load->model("itensCompra");
        $this->load->model("pessoas");
        $todas = $this->compras->getPagos();
        foreach($todas as $compra){
            $compra->produtor = $this->pessoas->get($compra->pessoa_cpf);
            $compra->itens = $this->itensCompra->get($compra->id);
            @$compra->total->verde = $this->compras->get_totais($compra->id,'VERDE');
            @$compra->total->madura = $this->compras->get_totais($compra->id,'MADURA');
            $compra->totalTudo = $compra->total->verde->peso + $compra->total->madura->peso;
            $compra->desconto = 0;
            $this->compras->setId($compra->id);
            $desconto = $this->compras->getDescontos();
            if (count($desconto)){
                foreach ($desconto as $des){
                    $compra->desconto  += $des->valor;
                    $compra->totalTudo -= $des->valor;
                }
            }
        }
        $this->load->view("cabecalho");
        $this->load->view("listaPagos",["todas"=>$todas]);
        $this->load->view("rodape");
    }

}