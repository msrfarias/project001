<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 16/02/2018
 * Time: 15:20
 */

class Login extends CI_Controller
{

    public function g($pss){
        echo md5($pss);
    }

    public function index()
    {
        $this->authenticate("",true);
        $this->load->view("login");
    }

    private function authenticate($r="",$adm=false){
        if (!empty($_POST["user"]) && !empty($_POST["pass"])) {
            $user = addslashes($_POST["user"]);
            $pass = md5($_POST["pass"]);
            $this->load->model("funcionarios");
            //$this->funcionarios = new Funcionarios();
            $this->funcionarios->setCpf($user);
            $this->funcionarios->setSenha($pass);
            if ($this->funcionarios->auth($adm)) {
                $this->session->set_userdata("logado", true);
                $this->session->set_userdata("key", "CkvfG7s7blGj8HGkGzk79HOTiHQOttk2");
                $func = $this->funcionarios->get($user);
                $this->session->set_userdata("nome", $func->nome);
                $this->session->set_userdata("user", $func->cpf);
                header("Location: ".URL_PUBLIC."/".$r);
            }
            return false;
        }
        return true;
    }


    public function app(){
        $this->authenticate("coleta/app");
        $this->load->view("login",["app"=>true]);
    }


}