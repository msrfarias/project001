<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 21/12/2017
 * Time: 01:17
 */

require_once "Restrict.php";

class Lote extends Restrict
{


    public function excluir(){

        if (empty($_POST["id"]))
            return;
        $id = $_POST["id"];
        $cpf = $_POST["cpf"];
        $this->load->model("lotes");
        $this->lotes->delete($cpf,$id);
    }


}