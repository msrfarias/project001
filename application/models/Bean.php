<?php

interface Bean{

    function save();
    function delete($id);
    function get($params);

}