<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div id="success"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Relatório de Adiantamentos <small></small></h2>
                    <ul class="nav navbar-left panel_toolbox">
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="search" id="search" class="form-control" placeholder="Buscar" style="width:200px; ">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Buscar</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="add-on" href="<?=URL_PUBLIC?>/adiantamento/cadastro"><i class="fa fa-plus"></i></a>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">



                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action table-hover table-condensed" id="table">
                            <thead >
                            <tr class="headings">
                                <th class="column-title">Produtor</th>
                                <th class="column-title">Data</th>
                                <th class="column-title">Valor</th>
                                <th class="column-title">Restante</th>
                                <th class="column-title no-link last"><span class="nobr"></span>
                            </tr>

                            </thead>
                            <tbody>
                            <?php foreach ($adiantamentos as $adiantamento):?>
                                <tr class="even pointer accordion-toggle linha linhaC" data-toggle="collapse" data-parent="#OrderPackages" data-target=".packageDetails1<?=$adiantamento->id ?>">
                                    <td><?= $adiantamento->produtor->nome?></td>
                                    <td><?= date('d-m-Y',  strtotime($adiantamento->data))?></td>
                                    <td><?= $adiantamento->valor?></td>
                                    <td><?= $adiantamento->restante?></td>
                                    <td class=" "><a class="verRecibo" href="<?=URL_PUBLIC?>/adiantamento/recibo/" cod="<?=$adiantamento->id?>" id="btnRecibo""><i class="success fa fa-file-text " style="margin-left:40%"></i></a></td>
                                </tr>
                                <tr class="hiddenRow linhaEscondida">

                                    <td colspan="3">
                                        <?php if (count($adiantamento->parcelas)):?>

                                         <div class="accordion-body collapse packageDetails1<?= $adiantamento->id ?>">
                                            <table class="table jambo_table">

                                                <head>
                                                    <tr>
                                                        <th  colspan="3"><h6><b>Parcelas pagas:</b></h6></th>
                                                    </tr>
                                                    <tr>
                                                        <th>Data</th>
                                                        <th>Valor</th>
                                                        <th></th>
                                                    </tr>
                                                </head>
                                                <tbody>
                                                    <?php foreach ($adiantamento->parcelas as $parcela):?>
                                                    <tr>
                                                        <td><?=date("d-m-Y",strtotime($parcela->data))?></td>
                                                        <td>R$ <?= number_format($parcela->valor,2,","," ")?></td>
                                                        <td><a class="ParcelasPg" href="#" Parcela-compra="<?= $parcela->compra?>" onclick="getDadosRecibo()">recibo</a></td>
                                                    </tr>
                                                    <?php endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php endif?>
                                    </td>
                                    <td colspan="3"></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>


                        </table>
                        <div id="modal_reciboA" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Recibo de Adiantamento <strong><span id="nome_delete"></span></strong></h4>
                                    </div>
                                    <form>
                                        <div class="modal-body">
                                            <div class="panel panel-default">
                                                <div class="panel-body" id="">
                                                    <div id="reciboAd"> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button id="gerarAdiantamento" class="btn btn-primary">Gerar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="modal_recibo" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Recibo<strong><span id="nome_delete"></span></strong></h4>
                                    </div>
                                    <form>
                                        <div class="modal-body">
                                            <div class="panel panel-default">
                                                <div class="panel-body" id="recibo-modal">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button id="gerarCompra" class="btn btn-primary">Gerar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
