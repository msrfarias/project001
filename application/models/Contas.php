<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 26/01/2018
 * Time: 12:13
 */

require_once "Bean.php";

class Contas extends CI_Model implements Bean
{

    private $conta;
    private $pessoa;
    private $banco;
    private $agencia;

    function save()
    {
        $this->load->database();
        $data = [
            "pessoa"     => $this->getPessoa(),
            "banco"      => $this->getBanco(),
            "agencia"    => $this->getAgencia(),
            "conta"      => $this->getConta(),
        ];
        $this->db->insert(_BD_."contas",$data);
    }

    function delete($cpf)
    {
        $this->load->database();
        $dados = [
            "pessoa" => $cpf
        ];
        $this->db->delete(_BD_.'contas',$dados);
    }

    function get($cpf)
    {
        $this->load->database();
        $query = "select * from "._BD_."contas where pessoa='$cpf'";
        $result = $this->db->query($query);
        if ($result->num_rows() < 1)
            return null;
        $res = array();
        $keys = get_object_vars(($result->result())[0]);

        foreach ($result->result() as $row){
            $obj = new stdClass();
            foreach ($keys as $key=>$val){
                $obj->$key = $row->$key;
            }
            $res[] = $obj;
        }
        return $res;
    }


    /**
     * @return mixed
     */
    public function getConta()
    {
        return $this->conta;
    }

    /**
     * @param mixed $conta
     */
    public function setConta($conta)
    {
        $this->conta = $conta;
    }

    /**
     * @return mixed
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @param mixed $pessoa
     */
    public function setPessoa($pessoa)
    {
        $this->pessoa = $pessoa;
    }

    /**
     * @return mixed
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * @param mixed $banco
     */
    public function setBanco($banco)
    {
        $this->banco = $banco;
    }

    /**
     * @return mixed
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * @param mixed $agencia
     */
    public function setAgencia($agencia)
    {
        $this->agencia = $agencia;
    }




}