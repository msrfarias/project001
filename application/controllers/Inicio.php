<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 11/12/2017
 * Time: 14:28
 */

require_once "Restrict.php";

class Inicio extends Restrict {

    private function cotacaoVazia($tipo){
        $cot = new stdClass();
        $cot->tipo = $tipo;
        $cot->valor = 0.00;
        $cot->semana = date("W");
        $cot->ano    = date("Y");
        $cot->id     = 0;
        return $cot;
    }

    public function index(){
        $cotacao = new stdClass();
        $this->load->model("cotacoes");
        $this->load->model("pessoas");
        $cotacao->verde = $this->cotacoes->getUltimaValida("VERDE");
        $cotacao->madura= $this->cotacoes->getUltimaValida("MADURA");
        $cotacao->verde = ($cotacao->verde)?($cotacao->verde):$this->cotacaoVazia("VERDE");
        $cotacao->madura = ($cotacao->madura)?($cotacao->madura):$this->cotacaoVazia("MADURA");
        $melhores = $this->pessoas->melhores();
        $this->load->view("cabecalho");
        $this->load->view("inicio",["cotacao" => $cotacao,"melhores"=>$melhores]);
        $this->load->view("rodape");
    }

}