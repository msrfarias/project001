<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 26/01/2018
 * Time: 23:41
 */

class Funcionarios extends CI_Model
{

    private $cpf;
    private $nome;
    private $telefone;
    private $endereco;
    private $senha;
    private $adm = false;
    function save()
    {
        if ($this->get($this->cpf)!= null)
            return false;
        $this->load->database();
        $data = [
            "cpf"        => $this->getCpf(),
            "nome"       => $this->getNome(),
            "telefone"   => $this->getTelefone(),
            "endereco"   => $this->getEndereco(),
            "senha"      => md5(explode(" ",trim($this->getNome()))[0].substr($this->getCpf(),0,3)),
            "level"      => $this->adm?"adm":"comum"
         ];
        return $this->db->insert(_BD_."funcionarios",$data);
    }

    function setAsADM(){
        $this->adm = true;
    }

    function auth($adm = false){
        $funcionario = $this->get($this->getCpf());
        if (!$funcionario)
            return false;
        if ($funcionario->senha == $this->getSenha()){
            if ($adm){
                return $funcionario->level === "adm";
            }
            return true;
        }
        return false;
    }


    function delete()
    {
        if (!$this->get($this->cpf))
            return false;
        $this->load->database();
        $this->db->where("cpf",$this->cpf);
        $this->db->from(_BD_."funcionarios");
        return $this->db->delete();
    }

    function  getAll(){
        $this->load->database();
        $this->db->from(_BD_."funcionarios");
        $this->db->where("cpf !=","SYSDBA");
        $this->db->order_by("nome","asc");
        $result = $this->db->get();
        $res = [];
        $keys = get_object_vars(($result->result())[0]);

        foreach ($result->result() as $row){
            $obj = new stdClass();
            foreach ($keys as $key=>$val){
                $obj->$key = $row->$key;
            }
            $res[] = $obj;
        }
        return $res;
    }

    function get($cpf)
    {
        $this->load->database();
        $result = $this->db->get_where(_BD_."funcionarios",["cpf"=>$cpf]);
        if ($result->num_rows())
            return $result->result()[0];
        return null;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return mixed
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * @param mixed $endereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return mixed
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param mixed $senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
    }





}