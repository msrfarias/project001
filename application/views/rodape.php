<footer>
    <div class="pull-right">
        Copyrigth by <a href="https://www.facebook.com/italo.andre.330">Italo André</a> e <a href="https://www.facebook.com/uesley.dasilvamelo">Uesley Melo</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?=URL_PUBLIC?>/vendors/jquery/dist/jquery.min.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/jquery.inputmask/dist/jquery.inputmask.bundle.js"></script>

<!-- Bootstrap -->
<script src="<?=URL_PUBLIC?>/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<!--<script src="--><?//=URL_PUBLIC?><!--/vendors/fastclick/lib/fastclick.js"></script>-->
<!-- NProgress -->
<!--<script src="--><?//=URL_PUBLIC?><!--/vendors/nprogress/nprogress.js"></script>-->
<!-- Chart.js -->
<script src="<?=URL_PUBLIC?>/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<!--<script src="--><?//=URL_PUBLIC?><!--/vendors/gauge.js/dist/gauge.min.js"></script>-->
<!-- bootstrap-progressbar -->
<script src="<?=URL_PUBLIC?>/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<!--<script src="--><?//=URL_PUBLIC?><!--/vendors/iCheck/icheck.min.js"></script>-->
<!-- Skycons -->
<!--<script src="--><?//=URL_PUBLIC?><!--/vendors/skycons/skycons.js"></script>-->
<!-- Flot -->
<script src="<?=URL_PUBLIC?>/vendors/Flot/jquery.flot.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/Flot/jquery.flot.pie.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/Flot/jquery.flot.time.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/Flot/jquery.flot.stack.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?=URL_PUBLIC?>/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?=URL_PUBLIC?>/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<!--<script src="--><?//=URL_PUBLIC?><!--/vendors/jqvmap/dist/jquery.vmap.js"></script>-->
<!--<script src="--><?//=URL_PUBLIC?><!--/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>-->
<!--<script src="--><?//=URL_PUBLIC?><!--/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>-->
<!-- bootstrap-daterangepicker -->
<script src="<?=URL_PUBLIC?>/vendors/moment/min/moment.min.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/validator/jquery.validate.min.js"></script>
<script src="<?=URL_PUBLIC?>/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?=URL_PUBLIC?>/build/js/custom.min.js"></script>
<script src="<?=URL_PUBLIC?>/js/jquery-searchable-master/jquery.searchable.js"></script>
<script src="https://rawgit.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.min.js"></script>
<script src="<?=URL_PUBLIC?>/js/recibo.js"></script>
<script src="<?=URL_PUBLIC?>/js/sendForms.js"></script>
<script src="<?=URL_PUBLIC?>/js/controladorGrafico.js"></script>
<script src="<?=URL_PUBLIC?>/js/jquery.mask.min.js"></script>

</body>
</html>
