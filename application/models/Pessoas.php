<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 11/12/2017
 * Time: 14:54
 */

require_once "Bean.php";

class Pessoas extends CI_Model implements Bean{

    private $cpf = "";
    private $nome = "";
    private $telefone = "";
    private $banco = "";
    private $agencia = "";
    private $conta = "";
    private $produtor = "";
    private $observacao = "";
    private $email = "";

    public function save(){
        if ($this->get($this->cpf)!= null)
            return false;
        $this->load->database();
        $data = [
            "cpf"        => $this->getCpf(),
            "nome"       => $this->getNome(),
            "telefone"   => $this->getTelefone(),
            "produtor"   => $this->getProdutor(),
            "observacao" => $this->getObservacao(),
            "email"      => $this->getEmail()
        ];
        $r = $this->db->insert(_BD_."pessoas",$data);
        return $r;
    }

    public function delete($cpf){
        $this->load->database();
        $query = $this->db->get_where(_BD_.'pessoas',array('cpf' => $cpf));
        if ($query->num_rows() > 0) {
            $this -> db -> where('cpf', $cpf);
            $this -> db -> delete(_BD_.'pessoas');
            return true;
        }
        return false;
    }


    public function update($cpf){
        $dados = [
            "nome"      => $this->getNome(),
            "telefone"  => $this->getTelefone(),
            "email"     => $this->getEmail(),
            "observacao"=> $this->getObservacao()
        ];
        $this->load->database();
        $this->db->where('cpf', $cpf);
        $this->db->update(_BD_.'pessoas', $dados);
    }


    public function get($cpf){
        $this->load->database();
        $result = $this->db->get_where(_BD_."pessoas",["cpf"=>$cpf]);
        if ($result->num_rows())
            return $result->result()[0];
        return [];
    }

    public function getAllProdutores(){
        $this->load->database();
        $this->db->from(_BD_."pessoas");
        $this->db->where("produtor",'PRODUTOR');
        $this->db->order_by("nome","asc");
//        $query = "select * from pessoas where produtor='PRODUTOR'";
//        $result = $this->db->query($query);
        $result = $this->db->get();
        $res = [];
        @$keys = get_object_vars(($result->result())[0]);

        foreach ($result->result() as $row){
            $obj = new stdClass();
            foreach ($keys as $key=>$val){
                $obj->$key = $row->$key;
            }
            $res[] = $obj;
        }
        return $res;
    }

    private function totaisTipoMes($tipo,$ano,$mes){

        $this->load->database();
        $this->db->select("SUM(caixas) as tcaixa");
        $this->db->select("SUM(peso) as tpeso");
        $this->db->from(_BD_."compras as c");
        $this->db->join(_BD_."itens_compra as ic","c.id = ic.compra_id");
        $this->db->where("status","PAGO");
        $this->db->where("month(data)",$mes);
        $this->db->where("year(data)",$ano);
        $this->db->where("tipo",$tipo);
        return $this->db->get()->result()[0];
    }

    private function melhoresTipoMes($tipo,$ano,$mes){
        $this->load->database();
        $this->db->select("cpf");
        $this->db->select("nome");
        $this->db->select("tipo");
        $this->db->select("SUM(caixas) as caixas");
        $this->db->select("SUM(peso) as peso");
        $this->db->from(_BD_."compras as c");
        $this->db->join(_BD_."pessoas","cpf = pessoa_cpf");
        $this->db->join(_BD_."itens_compra as ic","c.id = ic.compra_id");
        $this->db->where("status","PAGO");
        $this->db->where("month(data)",$mes);
        $this->db->where("year(data)",$ano);
        $this->db->where("tipo",$tipo);
        $this->db->group_by("cpf,tipo");
        $subquery = "(".$this->db->get_compiled_select().") as CdM";
        $this->db->from($subquery);
        $this->db->order_by("caixas","desc");
        $this->db->order_by("peso","desc");
        $this->db->limit(2);
        return $this->db->get()->result();
    }



    public function melhores(){
        //consulta de melhores
        $mes = date("m",time());
        $ano = date("Y",time());
        foreach (["VERDE","MADURA"] as $tipo){
            $totais[$tipo] = $this->totaisTipoMes($tipo,$ano,$mes);
            $melhores[$tipo] = $this->melhoresTipoMes($tipo,$ano,$mes);
            foreach ($melhores[$tipo] as $cara){
                //if ($tipo == "VERDE") {
                    $cara->prct = ($cara->peso) / ($totais[$tipo]->tpeso) * 100;
                  //  continue;
                //}
                //$cara->prct = ($cara->caixas)/($totais[$tipo]->tcaixa)*100;
            }
        }
//        echo "<pre>";
//        var_dump($melhores);
//
//        echo "</pre>";
//        exit();
        return $melhores;
    }


    public function getCpf()
    {
        return $this->cpf;
    }

    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    public function setBanco($banco)
    {
        $this->banco = $banco;
    }

    public function setAgencia($agencia)
    {
        $this->agencia = $agencia;
    }

    public function setConta($conta)
    {
        $this->conta = $conta;
    }

    public function setProdutor($produtor)
    {
        $this->produtor = $produtor;
    }

    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    public function getProdutor()
    {
        return $this->produtor;
    }

    public function getConta()
    {
        return $this->conta;
    }

    public function getAgencia()
    {
        return $this->agencia;
    }

    public function getBanco()
    {
        return $this->banco;
    }

    public function getTelefone()
    {
        return $this->telefone;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


}