<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 09/02/2018
 * Time: 12:25
 */

class Adiantamentos extends CI_Model
{

    private $id;
    private $valor;
    private $data;
    private $pessoa_cpf;
    private $restante;


    public function save(){
        $this->load->database();
        $dados = [
            "valor"  => $this->getValor(),
            "data"   => date("Y-m-d",time()),
            "pessoa_cpf" => $this->getPessoaCpf(),
            "restante" => $this->getValor()
        ];
        return $this->db->insert(_BD_."adiantamentos",$dados);
    }

    public function getLastId(){
        $this->load->database();
        $query = "select max(id) as id from "._BD_."adiantamentos";
        $r = $this->db->query($query);
        return ($r->result()[0])->id;
    }

    public function get($id= null){
        $this->load->database();
        $c = ["id" => $id];
        if (empty($id))
            $c = [];
         $result = $this->db->get_where(_BD_."adiantamentos",$c);
//        if ($result->num_rows())
            return $result->result();
//        return [];
    }

    public function getByCpf($cpf){
        $this->load->database();
        $this->db-> where('restante > 0', null, false);
        $r = $this->db->get_where(_BD_."adiantamentos",["pessoa_cpf"=>$cpf]);
        if ($r->num_rows())
            return $r->result();
        return [];
    }

    public function getParcelas($id){
        $this->load->database();
        $r = $this->db->get_where(_BD_."prestacoes",["adiantamento_id"=>$id]);
        if ($r->num_rows())
            return $r->result();
        return [];
    }

    public function addParcela($compra,$valor){
        $this->load->database();
        $dados = [
            "compra" => $compra,
            "adiantamento_id" => $this->getId(),
            "valor" => $valor,
            "data"  => date("Y-m-d", time())
        ];
        $restante = $this->db->get_where(_BD_."adiantamentos",["id"=>$this->getId()]);
        if ($restante->num_rows() == 0){
            echo "\nnaoRestante\n";
            return false;
        }
        $restante = $restante->result()[0];
        $restante = $restante->restante;
        if ($restante < $valor){
            echo "\n rst = $restante; vl = $valor\n";
            return false;
        }
        $this->db->insert(_BD_."prestacoes",$dados);
        $atualizar = [
            "restante" => $restante - $valor
        ];
        $this->db->where("id",$this->getId());
        return ($this->db->update(_BD_."adiantamentos",$atualizar));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getPessoaCpf()
    {
        return $this->pessoa_cpf;
    }

    /**
     * @param mixed $pessoa_cpf
     */
    public function setPessoaCpf($pessoa_cpf)
    {
        $this->pessoa_cpf = $pessoa_cpf;
    }

    /**
     * @return mixed
     */
    public function getRestante()
    {
        return $this->restante;
    }

    /**
     * @param mixed $restante
     */
    public function setRestante($restante)
    {
        $this->restante = $restante;
    }





}