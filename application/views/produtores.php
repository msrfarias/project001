<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Cadastro de Produtores <small>      </small></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form class="form-horizontal form-label-left input_mask" id="formProdutor" action="<?=isset($produtor)?"alterar/{$produtor->cpf}":"cadastroJa"?>" method="POST">
                            <input type="hidden" name="frm_produtor" value="1">
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="nome" class="form-control has-feedback-left" id="nome" placeholder="Nome" value="<?= isset($produtor)?$produtor->nome:''?>" required="required">
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="cpf" class="form-control has-feedback-left" id="cpf" placeholder="C.P.F" <?=isset($produtor)?"readonly":""?>  value="<?=isset($produtor)?$produtor->cpf:''?>" required="required"  pattern="\d{3}\.\d{3}\.\d{3}-\d{2}">
                                <span class="fa fa-indent form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback-right">
                                <input type="text" name="telefone" class="form-control has-feedback-left" id="telefone" placeholder="Fone"   value="<?=isset($produtor)?$produtor->telefone:''?>" required="required" >
                                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="email" class="form-control has-feedback-left" id="email" placeholder="Email" value="<?=isset($produtor)?$produtor->email:''?>">
                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <fieldset class="col-md-6" style="display: inline; float: right;">
                                <legend>
                                    Contas
                                </legend>
                                <div class="panel panel-default">
                                    <div class="panel-body" id="form_conta">

                                        <div id="" >
                                            <button id="addc" type="button" class="btn btn-default btn-sm  " aria-label="Right Align">
                                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                        <?php if (!empty($produtor->contas)): ?>
                                           <?php foreach($produtor->contas as $conta):?>
                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback " style="width: 32%;">
                                                    <input type="text" class="form-control has-feedback-left" id="banco" name="banco[]"  placeholder="Banco" value="<?=$conta->banco?>">
                                                    <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" style="width: 30%">
                                                    <input type="text" class="form-control" id="agencia" name="agencia[]" placeholder="Agencia" value="<?=$conta->agencia?>">
                                                    <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" style="width: 30%">
                                                    <input type="text" class="form-control" id="conta" name="conta[]" placeholder="Conta" value="<?=$conta->conta?>">
                                                    <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                                                </div>
                                            <?php endforeach ?>
                                         <?php else :?>
                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback " style="width: 32%;">
                                                <input type="text" class="form-control has-feedback-left" id="banco" name="banco[]"  placeholder="Banco">
                                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>

                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" style="width: 30%">
                                                <input type="text" class="form-control" id="agencia" name="agencia[]" placeholder="Agencia">
                                                <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" style="width: 30%">
                                                <input type="text" class="form-control" id="conta" name="conta[]" placeholder="Conta">
                                                <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        <?php endif;?>
                                        <div id="testando"></div>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="col-md-6" style="display: inline; float: left;">
                                <legend>Lotes</legend>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" >
                                            <select class="form-control" id="nucleo" name="nucleo">
                                                <option disabled selected>Selecione seu Núcleo</option>
                                                <option value="N1">N1</option>
                                                <option value="N2">N2</option>
                                                <option value="N3">N3</option>
                                                <option value="N4">N4</option>
                                                <option value="N5">N5</option>
                                                <option value="N6">N6</option>
                                                <option value="N7">N7</option>
                                                <option value="N8">N8</option>
                                                <option value="N9">N9</option>
                                                <option value="N10">N10</option>
                                                <option value="N11">N11</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" id="endereco" placeholder="Endereço">
                                            <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <input type="text" class="form-control" id="area" placeholder="Área Plantada">
                                            <span class="fa form-control-feedback right" aria-hidden="true">Ha</span>
                                        </div>

                                        <div id="">
                                            <button id="add" type="button" class="btn btn-default btn-sm  " aria-label="Right Align">
                                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                        <div id="escondidos">
                                            <?php if (isset($produtor)):?>
                                                <?php $i=0?>
                                                <?php foreach ($produtor->lotes as $lote):?>
                                                    <input id='nucleo<?=$i?>' type='hidden' name='nucleo[]' value='<?=$lote->nucleo?>'>
                                                    <input id='endereco<?=$i?>' type='hidden' name='endereco[]' value='<?=$lote->endereco?>'>
                                                    <input id='area<?=$i?>' type='hidden' name='area[]' value='<?=$lote->area?>'>
                                                    <?php $i++?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        </div>
                                        <table class="table table-striped jambo_table bulk_action" id="table-lotes">
                                            <thead>
                                            <tr class="headings">
                                                <th class="column-title">Núcleo</th>
                                                <th class="column-title">Endereço</th>
                                                <th class="column-title">Área Plantada </th>
                                                <th class="column-title no-link last"><span class="nobr">Ação</span>
                                                </th>
                                                <th class="bulk-actions" colspan="7">
                                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            <?php if (isset($produtor)){?>
                                                <?php $i=0?>
                                                <?php foreach ($produtor->lotes as $lote):?>
                                                    <tr class="even pointer" id='linha<?=$i?>'>
                                                        <td><?=$lote->nucleo?></td>
                                                        <td><?=$lote->endereco?></td>
                                                        <td><?=$lote->area?></td>
                                                        <td>
                                                            <a href="#" id="jq_remove<?=$i?>" onclick="removeTableRow(<?=$i?>)">
                                                                <i class="success fa fa-trash"></i></a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++ ?>
                                                <?php endforeach;?>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-9 col-xs-12">
                                    <textarea class="resizable_textarea form-control " id="observacao" name="observacao" placeholder="Digite aqui a obeservação sobre este produtor"><?= isset($produtor)?$produtor->observacao:""?></textarea>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <a href="<?=URL_PUBLIC?>"><button type="button"  class="btn btn-primary">Cancelar</button></a>
                                    <button class="btn btn-primary" type="reset">Limpar</button>
                                    <button type="submit" class="btn btn-success">Confirmar</button>
                                </div>
                            </div>
                            <div id="success"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
