<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Cadastro de Funcionários <small>      </small></h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form class="form-horizontal form-label-left" id="formFuncionario">
                            <div class="checkboxThree">
                                <input type="checkbox" value="1" name="adm" id="checkboxThreeInput" >
                                <label for="checkboxThreeInput"><center>nível</center></label>
                            </div>
                            <input type="hidden" name="cadastro" value="1">
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="nome" class="form-control has-feedback-left" id="nome" placeholder="Nome" value="<?= isset($funcionario)?$funcionario->nome:''?>" required="required">
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" name="cpf" class="form-control has-feedback-left" id="cpfuncionario" placeholder="C.P.F" <?=isset($funcionario)?"readonly":""?>  value="<?=isset($funcionario)?$funcionario->cpf:''?>" required="required"  data-inputmask="'mask': '999.999.999-99'" pattern="\d{3}\.\d{3}\.\d{3}-\d{2}">
                                <span class="fa fa-indent form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback-right">
                                <input type="text" name="telefone" class="form-control has-feedback-left" id="telefone" placeholder="Fone"   value="<?=isset($funcionario)?$funcionario->telefone:''?>" required="required" >
                                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback-right">
                                <input type="text" name="endereco" class="form-control has-feedback-left" id="endereco" placeholder="Endereço"   value="<?=isset($funcionario)?$funcionario->endereco:''?>" required="required" >
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>


                            <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <a href="<?=URL_PUBLIC?>"><button type="button"  class="btn btn-primary">Cancelar</button></a>
                                    <button class="btn btn-primary" type="reset">Limpar</button>
                                    <button type="submit" class="btn btn-success">Confirmar</button>
                                </div>
                            </div>

                            <div id="success"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>