<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Novo Adiantamento<small>      </small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form id="formadiantamento" class="form-horizontal form-label-left ">

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback" >
                                <select class="form-control selectProdutor"  name="produtor">
                                    <option disabled selected>Selecione o Produtor</option>
                                    <?php foreach ($produtores as $produtor):?>
                                        <option value="<?=$produtor->cpf?>"><?=$produtor->nome?></option>
                                    <?php  endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="cpfadiantamento" placeholder="C.P.F"  value="" required="required"  pattern="\d{3}\.\d{3}\.\d{3}-\d{2}" data-inputmask="'mask': '999.999.999-99'">
                                <span class="fa fa-indent form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-right" id="valor" placeholder="Valor" name="valor" required="required" >
                                <span class="fa  form-control-feedback right" aria-hidden="true">R$</span>
                            </div>
                            <fieldset class="col-md-6" style="margin-top: 0.5%; margin-right: 10%">
                                <legend>Adiantamento</legend>

                                <div class="panel panel-default">
                                    <div class="panel-body">

                                        <div id="escondido"></div>
                                        <table class="table table-striped jambo_table bulk_action" id="table-adiantamento">
                                            <thead>
                                            <tr class="headings">
                                                <th class="column-title">Data</th>
                                                <th class="column-title">Restante</th>

                                                </th>
                                                <th class="bulk-actions" colspan="7">
                                                    <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                            </fieldset>
                            <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <a href="<?=URL_PUBLIC?>"><button type="button" class="btn btn-primary">Cancelar</button></a>
                                    <button class="btn btn-primary" type="reset">Limpar</button>
                                    <button type="submit" class="btn btn-success">Confirmar</button>
                                </div>
                            </div>
                            <div id="success"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
