<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 22/02/2018
 * Time: 15:55
 */

require_once "Restrict.php";

class Logout extends Restrict
{

    private function limpaSessao(){
        $this->session->unset_userdata("logado");
        $this->session->unset_userdata("key");
        $this->session->unset_userdata("nome");
    }


    public function index(){
       $this->limpaSessao();
       header("Location: ".URL_PUBLIC);
    }


    public function app(){
        $this->limpaSessao();
        header("Location: ".URL_PUBLIC."/login/app");
    }


}