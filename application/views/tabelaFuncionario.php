<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div id="success"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Relatório de Funcionários <small></small></h2>
                    <ul class="nav navbar-left panel_toolbox">
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="search" id="search" class="form-control" placeholder="Buscar" style="width:200px; ">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Buscar</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="add-on" href="<?=URL_PUBLIC?>/funcionario/cadastro"><i class="fa fa-plus"></i></a>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">



                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action table-hover table-condensed" id="table">
                            <thead >
                            <tr class="headings">
                                <th class="column-title">Funcionario</th>
                                <th class="column-title">C.P.F</th>
                                <th class="column-title">Telefone</th>
                                <th class="column-title">Endereço</th>
                                <th class="column-title no-link last"><span class="nobr"></span>
                            </tr>

                            </thead>
                            <tbody>
                            <?php foreach ($funcionarios as $funcionario):?>
                            <tr class="linha">
                                <td class="jq_nome"><?= $funcionario->nome ?></td>
                                <td><?= $funcionario->cpf ?></td>
                                <td><?= $funcionario->telefone ?></td>
                                <td><?= $funcionario->endereco ?></td>
                                <td class=" "><a class="jq_delete" href="" cpf="<?=$funcionario->cpf?>"><i class="success fa fa-trash" style="margin-left:40%"></i></a></td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>


                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
