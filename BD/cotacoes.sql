-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11-Fev-2018 às 22:24
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `facerola`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cotacoes`
--

CREATE TABLE `cotacoes` (
  `id` int(11) NOT NULL,
  `tipo` enum('VERDE','MADURA') NOT NULL,
  `valor` decimal(10,2) UNSIGNED NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cotacoes`
--

INSERT INTO `cotacoes` (`id`, `tipo`, `valor`, `data`) VALUES
(1, 'VERDE', '3.00', '2018-02-11 04:14:47'),
(2, 'MADURA', '3.00', '2018-02-11 04:14:47'),
(3, 'VERDE', '2.00', '2018-02-11 04:14:47'),
(4, 'VERDE', '3.00', '2018-02-11 04:14:47'),
(5, 'VERDE', '2.00', '2018-02-11 04:14:47'),
(6, 'VERDE', '2.00', '2018-02-11 04:14:47'),
(7, 'VERDE', '3.00', '2018-02-11 04:14:47'),
(8, 'VERDE', '5.20', '2018-02-08 03:00:00'),
(9, 'VERDE', '3.40', '2018-02-08 08:00:00'),
(10, 'VERDE', '3.20', '2018-02-02 03:00:00'),
(11, 'MADURA', '1.00', '2018-02-02 03:00:00'),
(12, 'MADURA', '2.50', '2018-02-06 03:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cotacoes`
--
ALTER TABLE `cotacoes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cotacoes`
--
ALTER TABLE `cotacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
