<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 21/12/2017
 * Time: 09:25
 */

class Compras extends CI_Model
{

    private $id;
    private $data;
    private $status;
    private $pessoa_cpf;
    private $funcionario_cpf;

    function save()
    {
        $data = [
            "pessoa_cpf" => $this->getPessoaCpf(),
            "funcionario_cpf" => $this->getFuncionarioCpf()
        ];
        $this->load->database();
        $this->db->insert(_BD_."compras",$data);
        $this->setId($this->db->insert_id());
        $this->alteraStatus("COLETA");
//        $this->cadastraEvento($this->getStatus());
    }


    function get_totais($id,$tipo){
        $this->load->database();
        $this->db->select("id");
        $this->db->select("tipo");
        $this->db->select("caixas");
        $this->db->select("peso");
        $this->db->select("cotacao");
        $this->db->select("compra_id");
        $this->db->select("(caixas * cotacao) as total_caixa");
        $this->db->select("(peso * cotacao) as total_peso");
        $this->db->from(_BD_."itens_compra");
        $subquery = $this->db->get_compiled_select();
        $this->db->select("C.id as id");
        $this->db->select("tipo");
        $this->db->select("SUM(total_caixa) as caixa");
        $this->db->select("SUM(total_peso) as peso");
        $this->db->from(_BD_."compras as C");
        $this->db->join("($subquery) as I",'C.id = I.compra_id');
        $this->db->where("C.id", $id);
        $this->db->where("tipo", $tipo);
        $res =  $this->db->get();
        if ($res->num_rows() > 0 )
            return $res->result()[0];
        $r = new stdClass();
        $r->caixa = 0;
        $r->peso = 0;
        return $r;
    }

    private function cadastraEvento($status){
        $this->load->database();
        $dados_evento = [
            "status" => $status,
            "data"   => date("Y-m-d",time()),
            "compra" => $this->getId()
        ];
        $this->db->insert(_BD_."eventos",$dados_evento);
    }

    private function alteraStatus($novoStatus){
        $this->load->database();
        $alteracao = [
            "status" => $novoStatus,
            "data"   => date("Y-m-d",time())
        ];
        $this->db->where('id',$this->getId());
        $this->db->update(_BD_.'compras',$alteracao);
        $this->cadastraEvento($novoStatus);
    }

    function pagar(){
        $this->alteraStatus("PAGO");
    }

    function validarColeta(){
        $this->alteraStatus("AGUARDO");
    }

    function invalidarColeta(){
        $this->alteraStatus("INVALIDO");
    }

    function delete($id){
        $this->setId($id);
        $this->alteraStatus("INVALIDO");
    }

    public function getDescontos(){
        $this->load->database();
        $this->db->where("compra",$this->getId());
        $this->db->from(_BD_."prestacoes");
        $r = $this->db->get();
        if ($r->num_rows() > 0){
            return $r->result();
        }
        return [];
    }

    private function get($params=null,$status = "AGUARDO"){
        $this->load->database();
        if ($params == null){
            $todos = $this->db->get_where(_BD_."compras",["status"=>$status]);
            if ($todos->num_rows() > 0)
                return $todos->result();
            return [];
        }
        foreach ($params as $key => $val){
            $this->db->where($key,$val);
        }
        $this->db->where("status", $status);
        $compra = $this->db->get(_BD_."compras");
        return $compra->result();
    }

    function getCompras($id=null){
        if ($id)
            return $this->get(["id" =>$id],"AGUARDO");
        return $this->get(null,"AGUARDO");
    }

    function getComprasByCPF($cpf){
        return $this->get(["cpf" =>$cpf],"AGUARDO");
    }

    function getPagos($id=null){
        if ($id)
            return $this->get(["id"=>$id],"PAGO");
        return $this->get(null,"PAGO");
    }

    function getCompra($id){
        $this->load->database();
        $this->db->where("id", $id);
        $compra = $this->db->get(_BD_."compras");
        return $compra->result();
    }

    function getColetas($id=null){
        if ($id)
            return $this->get(["id"=>$id],"COLETA");
        return $this->get(null,"COLETA");
    }

    function getCancelados($id=null){
        return $this->get(["id"=>$id],"INVALIDO");
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getPessoaCpf()
    {
        return $this->pessoa_cpf;
    }

    /**
     * @param mixed $pessoa_cpf
     */
    public function setPessoaCpf($pessoa_cpf)
    {
        $this->pessoa_cpf = $pessoa_cpf;
    }

    /**
     * @return mixed
     */
    public function getFuncionarioCpf()
    {
        return $this->funcionario_cpf;
    }

    /**
     * @param mixed $funcionario_cpf
     */
    public function setFuncionarioCpf($funcionario_cpf)
    {
        $this->funcionario_cpf = $funcionario_cpf;
    }




}