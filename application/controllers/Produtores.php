<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 15/12/2017
 * Time: 16:36
 */

require_once "Restrict.php";

class Produtores extends Restrict
{

    public function index(){
        $this->load->model("pessoas");
        $this->load->model("lotes");
        $tabela = $this->pessoas->getAllProdutores();
        foreach ($tabela as $produtor)
            $produtor->lotes = $this->lotes->get($produtor->cpf);
        $this->load->view("cabecalho");
        $this->load->view("tabelaProdutores",["produtores" => $tabela]);
        $this->load->view("modal_confirm",["pessoa"=>"produtores"]);
        $this->load->view("rodape");
    }

    public function remover(){
        if (empty($_POST["cpf"])){
            http_response_code(500);
            echo "Nao foi possivel atender esta requisição";
            return;
        }
        $this->load->model("pessoas");
        if ($this->pessoas->delete($_POST['cpf'])){
            http_response_code(200);
            echo "Produtor deletado com sucesso";
            return;
        }
        http_response_code(500);
        echo "Houve um erro inesperado ao tentar excluir";
    }

    private function verificaPOST($vars){
        $post = new stdClass();
        $post->valid = true;

        // coloca $_POST em $post
        foreach ($_POST as $name=>$value)
            $post->$name = $_POST["$name"];

        /// verifica campos obrigatorios
        foreach ($vars as $item){
            if (!empty($post->$item))
                continue;
            $post->valid = false;
        }
        return $post;
    }


    public function cadastroJa(){
        $post = $this->verificaPOST(["cpf","nome","telefone"]);
        if ($post->valid){
            $this->load->model("pessoas");
            $this->pessoas->setNome($post->nome);
            $this->pessoas->setCpf( $post->cpf);
            $this->pessoas->setTelefone( $post->telefone);
            $this->pessoas->setProdutor("PRODUTOR");
            $this->pessoas->setEmail($post->email);
            $this->pessoas->setObservacao($post->observacao);
            if (!$this->pessoas->save()){
                http_response_code(406);
                echo "Este Produtor já está cadastrado";
                return;
            }
            http_response_code(200);
            $this->load->model("lotes");
            if (!empty($post->area)) {
                for ($i = 0; $i < count($post->area); $i++) {
                    $this->lotes->setNucleo($post->nucleo[$i]);
                    $this->lotes->setArea($post->area[$i]);
                    $this->lotes->setEndereco($post->endereco[$i]);
                    $this->lotes->setDonoCpf($post->cpf);
                    $this->lotes->save();
                }
            }
            $this->load->model("contas");
            if (!empty($post->conta)){
                for($i=0;$i<count($post->conta);$i++){
                    $this->contas->setConta($post->conta[$i]);
                    $this->contas->setBanco($post->banco[$i]);
                    $this->contas->setAgencia($post->agencia[$i]);
                    $this->contas->setPessoa($post->cpf);
                    $this->contas->save();
                }
            }
            echo "Cadastrado com Sucesso";
            return;
        }
        http_response_code(406);
        echo "Dados insuficientes para cadastrar";
    }

    public function alterar($cpf){
        $post = $this->verificaPOST(["cpf","nome","telefone"]);
        if ($post->valid) {
            $this->load->model("pessoas");
            $this->pessoas = new Pessoas();
            //$this->pessoas->setCpf($post->cpf);
            $this->pessoas->setNome($post->nome);
            $this->pessoas->setTelefone($post->telefone);
            $this->pessoas->setEmail($post->email);
            $this->pessoas->setObservacao($post->observacao);

            $this->pessoas->update($cpf);
            $this->load->model("lotes");
            if (!empty($post->area)) {
                $this->lotes->delete($cpf);
                for ($i = 0; $i < count($post->area); $i++) {
                    $this->lotes->setNucleo($post->nucleo[$i]);
                    $this->lotes->setArea($post->area[$i]);
                    $this->lotes->setDonoCpf($cpf);
                    $this->lotes->setEndereco($post->endereco[$i]);
                    $this->lotes->save();
                }
            }
            if (!empty($post->conta)){
                $this->load->model("contas");
                $this->contas->delete($cpf);
                for($i=0;$i < count($post->conta);$i++){
                    $this->contas->setPessoa($cpf);
                    $this->contas->setAgencia($post->agencia[$i]);
                    $this->contas->setBanco($post->banco[$i]);
                    $this->contas->setConta($post->conta[$i]);
                    $this->contas->save();
                }
            }
            return;
        }
        $this->load->model("pessoas");
        $this->load->model("lotes");
        $this->load->model("contas");
        $this->load->view("cabecalho");

        $p = $this->pessoas->get($cpf);
        $data = [];
        if ($p) {
            $p->lotes = $this->lotes->get($cpf);
            $p->contas = $this->contas->get($cpf);
            $data = ["produtor" => $p];
        }
        $this->load->view("produtores",$data);
        $this->load->view("rodape");
    }


    public function cadastro(){

        $this->load->view("cabecalho");
        $this->load->view("produtores");
        $this->load->view("rodape");

    }

}