<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 26/01/2018
 * Time: 23:03
 */

require_once "Restrict.php";

class Coleta extends Restrict
{

    public function index(){

    }

    public function app(){
        $this->load->model("pessoas","produtores");
        //$this->funcionarios = new Funcionarios();
        $funcionario = new stdClass();
        $funcionario->cpf = $this->session->user;
        $funcionario->nome = $this->session->nome;
        $listaFuncionarios= [$funcionario];

//        $this->produtores = new Pessoas();
        $listaProdutores = $this->produtores->getAllProdutores();
        $this->load->view("appHeader");

        $this->load->view("novacoleta",[
            "funcionarios" => $listaFuncionarios,
            "produtores"    => $listaProdutores,
            "app" => true
        ]);
        $this->load->view("rodape");
    }

    public function nova(){

        if (isset($_POST["coletaNova"])){
            if (!(empty($_POST['produtor']) || empty($_POST["funcionario"]))){
                $produtor = $_POST['produtor'];
                $funcionario = $_POST["funcionario"];
                $this->load->model("compras");
//                $this->compras = new Compras();
                $this->compras->setPessoaCpf($produtor);
                $this->compras->setFuncionarioCpf($funcionario);
                $this->compras->setData(date("Y-m-d",time()));
                $this->compras->save();
                $response = 200;
                $responseText = "Coleta Cadastrada com Sucesso.";
                $idCompra = $this->compras->getId();
                $this->load->model("itensCompra");
                $this->load->model("cotacoes");

                for ($i=0; $i< count($_POST["tipo"]); $i++){
                    $this->itensCompra->setTipo($_POST['tipo'][$i]);
                    $this->itensCompra->setCaixas(empty($_POST['quantidade'][$i])?0:$_POST['quantidade'][$i]);
                    $this->itensCompra->setPeso(empty($_POST['peso'][$i])?0:$_POST['peso'][$i]);
                    $this->itensCompra->setCompraId($idCompra);
                    $cot = $this->cotacoes->getUltimaVAlida($_POST['tipo'][$i]);
                    $this->itensCompra->setCotacao($cot->valor);
                    if (!($this->itensCompra->save())) {
                        $response = 500;
                        $responseText = "Houve um erro ao cadastrar os itens da coleta.";
                    }
                }
                http_response_code($response);
                echo $responseText;
                return;
            }
            http_response_code("406");
            echo "Dados insuficientes para cadastro";
            return;
        }

        $this->load->model("funcionarios");
        $this->load->model("pessoas","produtores");
        //$this->funcionarios = new Funcionarios();
        $listaFuncionarios = $this->funcionarios->getAll();
//        $this->produtores = new Pessoas();
        $listaProdutores = $this->produtores->getAllProdutores();
        $this->load->view("cabecalho");
        $this->load->view("novacoleta",[
                "funcionarios" => $listaFuncionarios,
                "produtores"    => $listaProdutores ]);
        $this->load->view("rodape");
    }

    public function listar(){
        $this->load->model("coletas");
        $listaColetas = $this->coletas->getAll();
        $this->load->model("funcionarios");
        $this->load->model("pessoas");
        $this->load->model("itensCompra");

        foreach ($listaColetas as $coleta){
            $p = $this->pessoas->get($coleta->pessoa_cpf);
            $f = $this->funcionarios->get($coleta->funcionario_cpf);
            if (!($p && $f))
                die("Houve um problema de referencias no banco de dados");
            $coleta->funcionario = $f?$f:$coleta->funcionario;
            $coleta->produtor = $p?$p:$coleta->produtor;
            $coleta->itens = $this->itensCompra->get($coleta->id);
            $total = 0;
            foreach($coleta->itens as $item){
                $total += $item->caixas;
            }
            $coleta->total = $total;

        }

//        echo "<pre>";
//        var_dump($listaColetas);
//        echo "</pre>";

        $this->load->view("cabecalho");
        $this->load->view("coletado",["coletas" => $listaColetas]);
        $this->load->view("modalConfirm");
        $this->load->view("rodape");
    }

    public function delete(){
        if (empty($_POST["id_coleta"])){
            http_response_code(500);
            echo "Esta coleta não está cadastrada";
            return;
        }
        $this->load->model("Compras","coletas");
//        $this->coletas = new Compras();
        $this->coletas->delete($_POST['id_coleta']);
        http_response_code(200);
        echo "Coleta deletada com sucesso";
    }

    public function deleteItem(){
        if (empty($_POST["id_item"])){
            http_response_code(500);
            echo "Este item não está cadastrado";
            return;
        }
        $this->load->model("itensCompra");
//        $this->itensCompra = new ItensCompra();
        $this->itensCompra->delete($_POST['id_item']);
        http_response_code(200);
        echo "Coleta deletada com sucesso";
    }

    public function validar()
    {
        if (empty($_POST["id_coleta"])) {
            http_response_code(500);
            echo "Esta coleta não está cadastrada";
            return;
        }
        $this->load->model("Compras", "coletas");
//        $this->coletas = new Compras();
        $this->coletas->setId($_POST['id_coleta']);
        $this->coletas->validarColeta();
        http_response_code(200);
        echo "Coleta validada com sucesso";
    }

    public function alteraItem(){
        if (empty($_POST["id_item"])) {
            http_response_code(500);
            echo "Esta coleta não está cadastrada";
            return;
        }
        $this->load->model("itensCompra","itens");
        $peso = isset($_POST["peso"])?$_POST["peso"]:0;
        $caixas = isset($_POST["caixas"])?$_POST["caixas"]:0;
        $this->itens->setPeso($peso);
        $this->itens->setCaixas($caixas);
        $this->itens->atualizarItem($_POST["id_item"]);
        http_response_code(200);
        echo "Item alterado com sucesso";
    }
}