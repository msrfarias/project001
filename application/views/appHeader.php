<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FA - Fernando Acerola </title>

    <!-- Bootstrap -->
    <link href="<?=URL_PUBLIC?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?=URL_PUBLIC?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <!-- iCheck -->

    <!-- bootstrap-progressbar -->
    <link href="<?=URL_PUBLIC?>/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <!--    <link href="--><?//=URL_PUBLIC?><!--/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>-->
    <!-- bootstrap-daterangepicker -->
    <link href="<?=URL_PUBLIC?>/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=URL_PUBLIC?>/build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=URL_PUBLIC?>/css/estilo.css">
</head>

<body class="nav-md">
