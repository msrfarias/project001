
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div id="success"></div>
<!--        --><?php
//        echo "<div><pre>";
//        var_dump($coletas);
//        echo "</pre></div>";
//        ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Coletado<small></small></h2>
                    <ul class="nav navbar-left panel_toolbox">
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="search" id="search" class="form-control" placeholder="Buscar Produtor" style="width:200px; ">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">Buscar</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </ul>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="add-on" href="<?=URL_PUBLIC?>/coleta/nova"><i class="fa fa-plus"></i></a>
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>

                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">



                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action table-hover table-condensed" id="table">
                            <thead>
                            <tr class="headings">
                                <th>

                                </th>
                                <th class="column-title">Funcionário</th>
                                <th class="column-title">Produtor </th>
                                <th class="column-title no-link last"><span class="nobr">Data</span>
                                <th class="column-title">Total</th>
                                <th class="column-title no-link last"><span class="nobr"></span>
                                </th>
                                <th class="bulk-actions" colspan="7">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($coletas as $coleta):?>
                                <tr class="even pointer accordion-toggle linha linhaC linhaF" data-toggle="collapse" data-parent="#OrderPackages" data-target=".packageDetails1<?= $coleta->id ?>">
                                    <td class="a-center ">

                                    </td>
                                    <td class=" "><?= $coleta->funcionario->nome ?></td>
                                    <td class=" "><?= $coleta->produtor->nome ?></td>
                                    <td class=" "><?= date('d-m-Y',  strtotime($coleta->data)) ?></td>
                                    <td class=" jq_nome"><?= $coleta->total ?></td>
                                    <td class=" ">
                                        <a class="jq_send" href="#">
                                            <form class="frm_confirm" message="Confirma a exclusao desta coleta?" titulo="Exclusão de coleta" action="<?=URL_PUBLIC?>/coleta/delete">
                                                <input type="hidden" value="<?= $coleta->id ?>" name="id_coleta">
                                            </form>
                                            <i class="success fa fa-trash"></i>
                                        </a>
                                        <a href="#" class="jq_send">
                                            <i class="success fa fa-check-square-o" style="margin-left:40%"></i>
                                            <form class="frm_confirm" message="Confirmar validação desta coleta?" titulo="Validação de Coleta" action="<?=URL_PUBLIC?>/coleta/validar">
                                                <input type="hidden" value="<?= $coleta->id ?>" name="id_coleta">
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                                <tr class="hiddenRow linhaEscondida">
                                    <td colspan="4" >
                                        <div class="accordion-body collapse packageDetails1<?= $coleta->id ?>" id="accordion1">
                                                <table class="table jambo_table">
                                                    <head>
                                                        <th>Tipo</th>
                                                        <th>Caixas</th>
                                                        <th>Peso</th>
                                                        <th>Ações</th>
                                                    </head>
                                                        <?php foreach($coleta->itens as $item):?>
                                                        <tr>
                                                            <td><?= $item->tipo ?></td>
                                                            <td><?= $item->caixas ?></td>
                                                            <td><?= $item->peso ?></td>
                                                            <td>
                                                                <a href="" class="jq_send" cod="">
                                                                    <form class="frm_confirm" message="Deseja excluir este item da coleta?" titulo="Exclusão de item" action="<?=URL_PUBLIC?>/coleta/deleteItem">
                                                                        <input type="hidden" value="<?=$item->id?>" name="id_item">
                                                                    </form>
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                                <a href="" class="jq_edit_coleta" valor="<?=$item->id?>" cod=""><i class="fa fa-edit" style="margin-left: 20%"></i>
                                                                    <input type="hidden" class="jq_edit_caixas" value="<?= $item->caixas ?>">
                                                                    <input type="hidden" class="jq_edit_pesos" value="<?= str_replace(",",".",$item->peso) ?>" >
                                                                </a>                                                            
                                                            </td>
                                                        </tr>
                                                        <?php endforeach;?>
                                                </table>
                                        </div>
                                    </td>
                                    <td colspan="2"></td>
                                </tr>

                            <?php endforeach ?>
                            </tbody>
                        </table>

                        <div id="modal_edit_coleta" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">


                                    <form id="frm_editColeta" class="form-horizontal form-label-left " action="<?=URL_PUBLIC?>/coleta/deleteItem" method="POST">
                                        <fieldset class="col-md-6" style="margin-top: 0.5%; margin-right: 10%">
                                            <legend>Coleta</legend>

                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <input type="hidden" class="form-control" id="jq_escondido" placeholder="Peso" name="id_item">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control" id="jq_peso_coleta" placeholder="Peso" name="peso">
                                                        <span class="fa form-control-feedback right" aria-hidden="true">Kg</span>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control" placeholder="Caixas" name="caixas" id="jq_caixas">
                                                        <span class="fa  form-control-feedback right" aria-hidden="true">Cx</span>
                                                    </div>
                                                </div>
                                        </fieldset>
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-primary ">Confirmar</button>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                    </div>

                </div>
            </div>
        </div>
    </div>
</div>