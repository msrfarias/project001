<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 20/12/2017
 * Time: 18:37
 */

require_once "Bean.php";

class Cotacoes extends CI_Model implements Bean
{

    private $id;
    private $tipo;
    private $valor;

    function save()
    {

        $this->load->database();
        $data = [
            "tipo"=> $this->getTipo(),
            "valor"=> $this->getValor()
        ];
        return $this->db->insert(_BD_."cotacoes",$data);
    }

    function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function getUltimaVAlida($tipo){
        $this->load->database();
        $query = "SELECT MAX(id) AS id FROM "._BD_."cotacoes WHERE tipo='$tipo'";
        $id = $this->db->query($query)->result()[0]->id;
        return ($id)?$this->get(["id"=>$id]):null;
    }

    function get($params)
    {
        $this->load->database();
        $query = $this->db->get_where(_BD_."cotacoes",$params);
        $result = $query->result();
        switch (count($result)){
            case 0 : return null;
            case 1 : return $result[0];
            default: return $result;
        }
    }

    function getLastCotacoes(){
        $tamanho = 10;
        $tipo = $this->getTipo();
//        $tipo = "VERDE";
        $query = "select * from "._BD_."cotacoes as c1 inner join (select max(id) as id,day(data) as dia, month(data) as mes, year(data) as ano from "._BD_."cotacoes where tipo = '$tipo' group by dia, mes, ano)as C2 on (c1.id = C2.id) where tipo='$tipo' order by C2.id desc limit 0,$tamanho";
        $this->load->database();
        $r = $this->db->query($query);
        $ultimas = array();
        $restante = $r->num_rows()-1;
        $r = array_reverse($r->result());
        for ($i=0;$i<$tamanho;$i++){
            $dh = date("d-m-Y",strtotime("-$i days",time()));
            $dh = new DateTime($dh);
            $ultimas[$i] = new stdClass();
            $ultimas[$i]->data = $dh;
            $ultimas[$i]->valor = "0.00";
            if ($restante+1){
                $topo = date("d-m-Y",strtotime($r[$restante]->data));
                $topo = new DateTime($topo);
                if ($topo->getTimestamp() > $ultimas[$i]->data->getTimestamp())
                    $restante--;
                if ($restante+1)
                    $ultimas[$i]->valor = $r[$restante]->valor;
            }
            $ultimas[$i]->dia = $ultimas[$i]->data->format("d");
            $ultimas[$i]->mes = $ultimas[$i]->data->format("m");
            $ultimas[$i]->ano = $ultimas[$i]->data->format("Y");
            unset($ultimas[$i]->data);
        }
        return $ultimas;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        if ($this->tipo != "VERDE" && $this->tipo != "MADURA")
            return "INVALIDO";
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }


    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

}