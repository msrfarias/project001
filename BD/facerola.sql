-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09-Fev-2018 às 20:39
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `facerola`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `adiantamentos`
--

CREATE TABLE `adiantamentos` (
  `id` int(11) NOT NULL,
  `valor` decimal(10,2) UNSIGNED DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `pessoa_cpf` varchar(15) NOT NULL,
  `restante` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `compras`
--

CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `status` enum('PAGO','AGUARDO','VALIDO','INVALIDO','COLETA') DEFAULT NULL,
  `pessoa_cpf` varchar(15) NOT NULL,
  `funcionario_cpf` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contas`
--

CREATE TABLE `contas` (
  `conta` varchar(15) NOT NULL,
  `banco` varchar(20) NOT NULL,
  `agencia` varchar(20) NOT NULL,
  `pessoa` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cotacoes`
--

CREATE TABLE `cotacoes` (
  `id` int(11) NOT NULL,
  `tipo` enum('VERDE','MADURA') NOT NULL,
  `valor` decimal(10,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `eventos`
--

CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `status` enum('PAGO','AGUARDO','VALIDO','INVALIDO','COLETA') DEFAULT 'COLETA',
  `data` date DEFAULT NULL,
  `compra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionarios`
--

CREATE TABLE `funcionarios` (
  `cpf` varchar(15) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `telefone` varchar(40) NOT NULL,
  `endereco` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens_compra`
--

CREATE TABLE `itens_compra` (
  `id` int(11) NOT NULL,
  `tipo` enum('VERDE','MADURA') DEFAULT NULL,
  `caixas` smallint(6) DEFAULT NULL,
  `peso` decimal(8,3) DEFAULT NULL,
  `cotacao` decimal(10,2) UNSIGNED DEFAULT NULL,
  `compra_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lotes`
--

CREATE TABLE `lotes` (
  `id` int(11) NOT NULL,
  `nucleo` varchar(45) DEFAULT NULL,
  `lote` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `area` decimal(8,3) DEFAULT NULL,
  `dono_cpf` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

CREATE TABLE `pessoas` (
  `cpf` varchar(15) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `produtor` enum('PRODUTOR','FUNCIONARIO') DEFAULT NULL,
  `observacao` varchar(500) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `prestacoes`
--

CREATE TABLE `prestacoes` (
  `id` int(11) NOT NULL,
  `valor` decimal(10,2) UNSIGNED DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `adiantamento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adiantamentos`
--
ALTER TABLE `adiantamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_adiantamentos_pessoa1_idx` (`pessoa_cpf`);

--
-- Indexes for table `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_compras_pessoa1_idx` (`pessoa_cpf`),
  ADD KEY `fk_compras_funcionarios1_idx` (`funcionario_cpf`);

--
-- Indexes for table `contas`
--
ALTER TABLE `contas`
  ADD PRIMARY KEY (`pessoa`,`conta`),
  ADD KEY `fk_contas_pessoas` (`pessoa`);

--
-- Indexes for table `cotacoes`
--
ALTER TABLE `cotacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`,`compra`),
  ADD KEY `fk_eventos_compras1_idx` (`compra`);

--
-- Indexes for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD PRIMARY KEY (`cpf`);

--
-- Indexes for table `itens_compra`
--
ALTER TABLE `itens_compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_itens_compra_compras1_idx` (`compra_id`);

--
-- Indexes for table `lotes`
--
ALTER TABLE `lotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lotes_pessoa_idx` (`dono_cpf`);

--
-- Indexes for table `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`cpf`);

--
-- Indexes for table `prestacoes`
--
ALTER TABLE `prestacoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_prestacoes_adiantamentos1_idx` (`adiantamento_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adiantamentos`
--
ALTER TABLE `adiantamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `cotacoes`
--
ALTER TABLE `cotacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `itens_compra`
--
ALTER TABLE `itens_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `lotes`
--
ALTER TABLE `lotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `prestacoes`
--
ALTER TABLE `prestacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `adiantamentos`
--
ALTER TABLE `adiantamentos`
  ADD CONSTRAINT `fk_adiantamentos_pessoa1` FOREIGN KEY (`pessoa_cpf`) REFERENCES `pessoas` (`cpf`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compras_funcionarios1` FOREIGN KEY (`funcionario_cpf`) REFERENCES `funcionarios` (`cpf`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compras_pessoa1` FOREIGN KEY (`pessoa_cpf`) REFERENCES `pessoas` (`cpf`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `contas`
--
ALTER TABLE `contas`
  ADD CONSTRAINT `fk_contas_pessoas` FOREIGN KEY (`pessoa`) REFERENCES `pessoas` (`cpf`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `eventos`
--
ALTER TABLE `eventos`
  ADD CONSTRAINT `fk_eventos_compras1` FOREIGN KEY (`compra`) REFERENCES `compras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `itens_compra`
--
ALTER TABLE `itens_compra`
  ADD CONSTRAINT `fk_itens_compra_compra` FOREIGN KEY (`compra_id`) REFERENCES `compras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `lotes`
--
ALTER TABLE `lotes`
  ADD CONSTRAINT `fk_lotes_pessoa` FOREIGN KEY (`dono_cpf`) REFERENCES `pessoas` (`cpf`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `prestacoes`
--
ALTER TABLE `prestacoes`
  ADD CONSTRAINT `fk_prestacoes_adiantamento` FOREIGN KEY (`adiantamento_id`) REFERENCES `adiantamentos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
