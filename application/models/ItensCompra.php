<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 21/12/2017
 * Time: 09:38
 */

require_once "Bean.php";

class ItensCompra extends CI_Model implements Bean
{
    private $id;
    private $tipo;
    private $caixas;
    private $peso;
    private $cotacao;
    private $compra_id;

    function save()
    {
        $data = [
          "tipo" => $this->getTipo(),
          "caixas" => $this->getCaixas(),
          "peso" =>   $this->peso>0?$this->peso:$this->getCaixas()*20,
          "cotacao" => $this->getCotacao(),
          "compra_id" =>$this->getCompraId()
        ];
        $this->load->database();
        return $this->db->insert(_BD_."itens_compra",$data);
    }

    function delete($id)
    {
        $this->load->database();
        $query = $this->db->get_where(_BD_.'itens_compra',array('id' => $id));
        if ($query->num_rows() > 0) {
            $this -> db -> where('id', $id);
            $this -> db -> delete(_BD_.'itens_compra');
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     * atualiza o numero de caixas e peso
     */
    function atualizarItem($id){
        $this->load->database();
        $dados = [
            "caixas" => $this->getCaixas(),
            "peso" => $this->getPeso()
        ];
        $query = $this->db->get_where(_BD_.'itens_compra',array('id' => $id));
        if ($query->num_rows() > 0) {
            $this -> db -> where('id', $id);
            $this -> db -> update(_BD_.'itens_compra',$dados);
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     * Atualiza o peso e a contação
     */
    function update($id){
        $this->load->database();
        $dados = [
            "cotacao" => $this->getCotacao(),
            "peso" => $this->getPeso()
        ];
        $query = $this->db->get_where(_BD_.'itens_compra',array('id' => $id));
        if ($query->num_rows() > 0) {
            $this -> db -> where('id', $id);
            $this -> db -> update(_BD_.'itens_compra',$dados);
            return true;
        }
        return false;
    }

    function get($compra_id)
    {
        $this->load->database();
        $this->db->select("id");
        $this->db->select("tipo");
        $this->db->select("caixas");
        $this->db->select("peso");
        $this->db->select("cotacao");
        $this->db->select("compra_id");
        $this->db->select("(caixas * cotacao) as total_caixa");
        $this->db->select("(peso * cotacao) as total_peso");
        $this->db->where('compra_id',$compra_id);
        $req = $this->db->get(_BD_."itens_compra");
        $r = $req->result();
        foreach ($r as $item){
            $item -> cotacao = number_format($item->cotacao,2,","," ");
            $item -> peso = number_format($item->peso,3,","," ");
            $item -> total_peso = number_format($item->total_peso,2,","," ");
            $item -> total_caixa = number_format($item->total_caixa,2,","," ");

        }
        return $r;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getCaixas()
    {
        return $this->caixas;
    }

    /**
     * @param mixed $caixas
     */
    public function setCaixas($caixas)
    {
        $this->caixas = $caixas;
    }

    /**
     * @return mixed
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * @param mixed $peso
     */
    public function setPeso($peso)
    {
        $this->peso = str_replace(",",".",$peso);
    }

    /**
     * @return mixed
     */
    public function getCotacao()
    {
        return $this->cotacao;
    }

    /**
     * @param mixed $cotacao
     */
    public function setCotacao($cotacao)
    {
        $this->cotacao = str_replace(",",".",$cotacao);
    }

    /**
     * @return mixed
     */
    public function getCompraId()
    {
        return $this->compra_id;
    }

    /**
     * @param mixed $compra_id
     */
    public function setCompraId($compra_id)
    {
        $this->compra_id = $compra_id;
    }




}