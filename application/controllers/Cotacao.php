<?php
/**
 * Created by PhpStorm.
 * User: Uesley Melo
 * Date: 20/12/2017
 * Time: 18:28
 */

require_once "Restrict.php";

class Cotacao extends Restrict
{

    private function verificaPOST($vars){
        $post = new stdClass();
        $post->valid = true;

        // coloca $_POST em $post
        foreach ($_POST as $name=>$value)
            $post->$name = $_POST["$name"];

        /// verifica campos obrigatorios
        foreach ($vars as $item){
            if (!empty($post->$item))
                continue;
            $post->valid = false;
        }
        return $post;
    }

    public function cadastro(){
        $post = $this->verificaPOST(["tipo","valor"]);

        $this->load->model("cotacoes");
        if ($post->valid){
            $post->valor = str_replace(",",".",$post->valor);
            $this->cotacoes->setTipo($post->tipo);
            $this->cotacoes->setValor($post->valor);
            if ($this->cotacoes->save()){
                http_response_code(200);
                $res = new stdClass();
                $res->message = "Cotação definida com Sucesso";
                $res->tipo = $post->tipo;
                $res->valor = $post->valor;
                echo json_encode($res);
                return;
            }
            http_response_code(500);
            echo "Houve um erro ao cadastrar";
            return;
        }
        http_response_code(406);
        echo "Dados insuficientes para atualização";
    }

    public function getUltimas(){
        $this->load->model("cotacoes");
        $result = new stdClass();
        $this->cotacoes->setTipo("VERDE");
        $result->verde = $this->cotacoes->getLastCotacoes();
        $this->cotacoes->setTipo("MADURA");
        $result->madura = $this->cotacoes->getLastCotacoes();
        echo json_encode($result);
    }


}