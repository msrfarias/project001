
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Acerola Verde</span>
            <div class="count" id="cotacaoVERDE"><?=$cotacao->verde->valor?></div>
            <span class="count_bottom"><i class="green">Kg </i> Hoje</span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-clock-o"></i> Acerola Vermelha</span>
            <div class="count" id="cotacaoMADURA"><?=$cotacao->madura->valor?></div>
            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>kg </i> Hoje</span>
        </div>

    </div>
    <!-- /top tiles -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Cotação <small>por</small> Dia</h3>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>

                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div id="graficoCotacoes" class="demo-placeholder"></div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                    <div class="x_title">
                        <h2>Melhores fornecedores</h2>
                        <div class="clearfix"></div>
                    </div>
                    <?php foreach ($melhores as $tipo=>$tops):?>

                    <div class="col-md-12 col-sm-12 col-xs-6">
                        <?php foreach ($tops as $top):?>
                        <div>
                            <p><?=$top->nome?>: (<?=number_format($top->prct,2,","," ")?>) %</p>
                            <div class="">
                                <div class="progress progress_sm" style="width: 76%;">
                                    <div class="progress-bar bg-<?=$tipo=="VERDE"?'green':'red'?>" role="progressbar" data-transitiongoal="<?=$top->prct?>"></div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                    <?php endforeach; ?>

                </div>

                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    <br />
