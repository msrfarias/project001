$(function () {
    init_flot_chart();
});

function getUltimasCotacoes(){
    var r;
    var a = {};
    a.verde = [];
    a.madura = [];

    $.ajax({
        url: "http://"+location.hostname+"/cotacao/getUltimas",
        async: false,
        success: function (data) {
            r = data;
        }
    });
    r = JSON.parse(r);
    for (i=0;i<r.verde.length;i++){
        a.verde.push([]);
        a.madura.push([]);
        a.verde [i][1] = parseFloat(r.verde[i].valor);
        a.madura[i][1] = parseFloat(r.madura[i].valor);
        a.verde [i][0] = new Date(r.verde[i].ano, r.verde[i].mes -1, r.verde[i].dia).getTime();
        a.madura[i][0] = new Date(r.madura[i].ano, r.madura[i].mes -1, r.madura[i].dia).getTime();

    }
    //console.log(a);
    return a;

}

function init_flot_chart(){

    if( typeof ($.plot) === 'undefined'){ return; }
    //console.log('init_flot_chart aki msm');

    dados = getUltimasCotacoes();
    var arrVerde = dados.verde;
    var arrMadura = dados.madura;
    //console.log(dados);
    var chart_plot_01_settings = {
        series: {
            lines: {
                show: false,
                fill: true
            },
            splines: {
                show: true,
                tension: 0.01,
                lineWidth: 1,
                fill: 0.4
            },
            points: {
                radius: 3,
                show: true
            },
            shadowSize: 1
        },
        grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
        },
        legend: {
            position: "ne",
            margin: [0, -25],
            noColumns: 4,
            labelBoxBorderColor: null,
            labelFormatter: function (label, series) {
                return label + '&nbsp;&nbsp;';
            },
            width: 40,
            height: 2
        },
        colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
        xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickLength: 10,
            axisLabel: "Dia",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
        },
        yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
            axisLabel: "Cotação",
        },
        tooltip: true
    };

    if ($("#graficoCotacoes").length){
        //console.log('Plot1');

        $.plot( $("#graficoCotacoes"), [
                {
                    label : "ACEROLA VERDE",
                    lines: {
                        fillColor: "rgba(150, 202, 89, 0.12)"
                    },
                    data : arrVerde,
                    points: {
                        fillColor: "#0f0"
                    }
                },
                {
                    label : "ACEROLA VERMELHA",
                    lines: {
                        fillColor: "rgba(150, 202, 89, 0.12)"
                    },
                    data : arrMadura,
                    points: {
                        fillColor: "#f00"
                    }
                }
            ],
            chart_plot_01_settings );
    }
}

$("<div id='tooltip'></div>").appendTo("body");

$("#graficoCotacoes").bind("plothover", function (event, pos, item) {

        if (item) {
            var x = item.datapoint[0].toFixed(2),
                y = item.datapoint[1].toFixed(2);

            var data = new Date();
            data.setTime(x);
            //console.log(item);
            meses = ['Janeiro',
                'Fevereiro',
                'Março',
                'Abril',
                'Maio',
                'Junho',
                'Julho',
                'Agosto',
                'Setembro',
                'Outubro',
                'Novembro',
                'Dezembro'];
            $("#tooltip").html(
                "Dia: " +data.getDate()+" de "+meses[data.getMonth()]+ "<br>"+
                "Cotação: R$ " + parseFloat(y).toFixed(2)+"<Br><strong>"+item.series.label+"</strong>")
                .css({top: item.pageY+5, left: item.pageX+5})
                .fadeIn(300);
        } else {
            $("#tooltip").hide();
        }
});